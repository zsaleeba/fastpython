#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include "antlr4-runtime.h"

#include "parser.h"
#include "parserexception.h"
#include "pyparservisitor.h"
#include "Python3Lexer.h"
#include "Python3Parser.h"
#include "Python3Visitor.h"
#include "ast.h"


namespace fastpython
{


Parser::Parser(const std::string &filename) : filename_(filename)
{
}


std::shared_ptr<Ast> Parser::parse()
{
    std::ifstream srcStream;
    srcStream.open(filename_);
    
    if (!srcStream.is_open())
        throw ParserException(std::string("can't open file '") + filename_ + "'");
    
    antlr4::ANTLRInputStream input(srcStream);
    Python3Lexer lexer(&input);
    
#if 0
    std::cout << "Tokens:\n";
    std::unique_ptr<antlr4::Token> tok;
    do 
    {
        tok = lexer.nextToken();
        std::cout << "<" << tok->toString() << ">\n";
    } while (tok->getType() != antlr4::Lexer::EOF);
    
    std::cout << std::endl;
    
//    tokens.reset();
#endif

#if 1
    antlr4::CommonTokenStream tokens(&lexer);

    Python3Parser parser(&tokens);
    
    Python3Parser::File_inputContext *tree = parser.file_input();
    PyParserVisitor visitor;
    antlrcpp::Any programAny = visitor.visitFile_input(tree);
    auto program = programAny.as<std::shared_ptr<Ast>>();
    //program->print(std::cout);
#endif
    
    return nullptr;
}


} // namespace fastpython.
