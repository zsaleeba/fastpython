#-------------------------------------------------
#
# Project created by QtCreator 2018-09-16T12:39:51
#
#-------------------------------------------------

QT       -= core gui

TARGET = parser
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++1z

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    /usr/local/include/antlr4-runtime \
    $$PWD/../common \
    $$PWD/gen

SOURCES += \
    parser.cpp \
    parserexception.cpp \
    pyparservisitor.cpp \
    ast.cpp \
    gen/Python3BaseVisitor.cpp \
    gen/Python3Lexer.cpp \
    gen/Python3Parser.cpp \
    gen/Python3Visitor.cpp

HEADERS += \
    parser.h \
    parserexception.h \
    pyparservisitor.h \
    ast.h \
    gen/Python3BaseVisitor.h \
    gen/Python3Lexer.h \
    gen/Python3Parser.h \
    gen/Python3Visitor.h \
    astvisitor.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    meson.build \
    Python3.g4
