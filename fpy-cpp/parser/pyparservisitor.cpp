#include <vector>
#include <memory>
#include <utility>

#include "pyparservisitor.h"
#include "ast.h"
#include "Python3Lexer.h"
#include "parserexception.h"


namespace fastpython {

    //
    // Gets a single statement.
    //

    antlrcpp::Any PyParserVisitor::visitSingle_input(Python3Parser::Single_inputContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // Gather all the statements in a file.
    //
    
    antlrcpp::Any PyParserVisitor::visitFile_input(Python3Parser::File_inputContext *ctx)
    {
        std::vector<AstStmtPtr> statements;
        statements.reserve(ctx->statements.size());

        for (auto statement : ctx->statements) 
        {                
            AstStmtPtr stmtAny = visitStmt(statement);
            auto stmtIncludedBlock = std::dynamic_pointer_cast<AstTmpIncludedBlock>(stmtAny);
            if (stmtIncludedBlock)
            {
                // It's a group of statements passed up from a simple_stmt 
                // which are to be included in this block.
                std::copy(stmtIncludedBlock->getChildren().begin(), stmtIncludedBlock->getChildren().end(), statements.end());
            }
            else
            {
                // It's just a single statement.
                statements.push_back(stmtAny);
            }
        }    

        return std::make_shared<AstStmtBlock>(statements);
    }
    
    
    //
    // Consists of a testlist and some newlines.
    //
    
    antlrcpp::Any PyParserVisitor::visitEval_input(Python3Parser::Eval_inputContext *ctx)
    {
        return visitTestlist(ctx->testlist());
    }
    
    
    //
    // decorator: '@' dotted_name ( '(' (arglist)? ')' )? NEWLINE
    //
    
    antlrcpp::Any PyParserVisitor::visitDecorator(Python3Parser::DecoratorContext *ctx)
    {
        auto dottedNames = visitDotted_name(ctx->dotted_name());
        
        std::shared_ptr<AstArgList> argList;
        auto arglistCtx = ctx->arglist();
        if (arglistCtx)
        {
            argList = visitArglist(arglistCtx);
        }
        
        return std::make_shared<AstDecorator>(dottedNames, argList);
    }
    
    
    //
    // decorators: decorator+
    //
    
    antlrcpp::Any PyParserVisitor::visitDecorators(Python3Parser::DecoratorsContext *ctx)
    {
        auto decoratorCtx = ctx->decorator();
        std::vector<AstDecoratorPtr> decorators;
        decorators.reserve(decoratorCtx.size());
        for (auto &ctx : decoratorCtx)
        {
            std::shared_ptr<AstDecorator> decorator = visitDecorator(ctx);
            decorators.push_back(decorator);
        }
        
        return std::move(decorators);
    }
    
    
    //
    // decorated: decorators (classdef | funcdef | async_funcdef)
    //
    
    antlrcpp::Any PyParserVisitor::visitDecorated(Python3Parser::DecoratedContext *ctx)
    {
        auto decorators = visitDecorators(ctx->decorators());
        auto classdefCtx = ctx->classdef();
        if (classdefCtx)
        {
            // Decorated class def.
            std::shared_ptr<AstStmtClassDef> classdef = visitClassdef(classdefCtx);
            classdef->setDecorators(decorators);
            
            return std::move(classdef);
        }
        else 
        {
            auto funcdefCtx = ctx->funcdef();
            if (funcdefCtx)
            {
                // Decorated func def.
                std::shared_ptr<AstStmtFuncDef> funcdef = visitFuncdef(funcdefCtx);
                funcdef->setDecorators(decorators);
                
                return std::move(funcdef);
            }
            else 
            {
                // Decorated async func def.
                std::shared_ptr<AstStmtFuncDef> funcdef = visitAsync_funcdef(ctx->async_funcdef());
                funcdef->setDecorators(decorators);
                
                return std::move(funcdef);
            }
        }
    }
    
    
    //
    // async_funcdef: ASYNC funcdef
    //
    
    antlrcpp::Any PyParserVisitor::visitAsync_funcdef(Python3Parser::Async_funcdefContext *ctx)
    {
        std::shared_ptr<AstStmtFuncDef> funcdef = visitFuncdef(ctx->funcdef());
        funcdef->setAsync(true);
        
        return std::move(funcdef);
    }
    
    
    //
    // funcdef: 'def' NAME parameters ('->' test)? ':' suite
    //
    
    antlrcpp::Any PyParserVisitor::visitFuncdef(Python3Parser::FuncdefContext *ctx)
    {
        AstExprPtr params = visitParameters(ctx->parameters());
        AstStmtPtr body = visitSuite(ctx->suite());
        AstExprPtr returns;

        auto returnsCtx = ctx->test();
        if (returnsCtx)
        {
            returns = visitTest(returnsCtx);
        }
        
        auto name = ctx->NAME()->getSymbol()->getText();
        return std::make_shared<AstStmtFuncDef>(false, name, params, returns, body);
    }
    
    
    //
    // parameters: '(' (typedargslist)? ')'
    //
    
    antlrcpp::Any PyParserVisitor::visitParameters(Python3Parser::ParametersContext *ctx)
    {
        auto paramsCtx = ctx->typedargslist();
        if (paramsCtx)
        {
            return visitTypedargslist(paramsCtx);
        }
        else
        {
            return std::make_shared<AstExprList>();
        }
    }
    
    
    //
    // typedargslist: (args+=tfpassign (',' args+=tfpassign)* (',' (
    //    '*' (sarg0=tfpdef)? (',' sargs+=tfpassign)* (',' ('**' ssarg=tfpdef (',')?)?)?
    //  | '**' ssarg=tfpdef (',')?)?)?
    //  | '*' (sarg0=tfpdef)? (',' sargs+=tfpassign)* (',' ('**' ssarg=tfpdef (',')?)?)?
    //  | '**' ssarg=tfpdef (',')?);
    //
    
    antlrcpp::Any PyParserVisitor::visitTypedargslist(Python3Parser::TypedargslistContext *ctx)
    {
        std::vector<AstArgList::Arg> args;
        std::vector<AstArgList::Arg> sargs;
        std::optional<AstArgList::Arg> ssarg;

        // Get the normal args.        
        args.reserve(ctx->args.size());
        
        for (auto argCtx : ctx->args)
        {
            args.emplace_back(visitTfpassign(argCtx));
        }
        
        // Get the star args.
        sargs.reserve(ctx->sargs.size() + (ctx->sarg0 ? 1 : 0));
        
        if (ctx->sarg0)
        {
            std::pair<std::string, AstExprPtr> sarg0 = visitTfpdef(ctx->sarg0);
            sargs.emplace_back(sarg0.first, sarg0.second, nullptr);
        }
        
        for (auto sargCtx : ctx->sargs)
        {
            sargs.emplace_back(visitTfpassign(sargCtx));
        }
        
        // Get the star star arg.
        if (ctx->ssarg)
        {
            std::pair<std::string, AstExprPtr> ssarg = visitTfpdef(ctx->ssarg);
            ssarg = {ssarg.first, ssarg.second};
        }
        
        return std::make_shared<AstArgList>(args, sargs, ssarg);
    }
    
    
    //
    // tfpassign: tfpdef ('=' test)?
    //
    
    antlrcpp::Any PyParserVisitor::visitTfpassign(Python3Parser::TfpassignContext *ctx)
    {
        std::pair<std::string, AstExprPtr> def = visitTfpdef(ctx->tfpdef());
        AstExprPtr initialiser;
        
        auto initialiserCtx = ctx->test();
        if (initialiserCtx)
        {
            initialiser = visitTest(initialiserCtx);
        }
        
        return AstArgList::Arg(def.first, def.second, initialiser);
    }
    
    
    //
    // tfpdef: NAME (':' test)?
    //
    
    antlrcpp::Any PyParserVisitor::visitTfpdef(Python3Parser::TfpdefContext *ctx)
    {
        std::string name = ctx->NAME()->getSymbol()->getText();
        AstExprPtr annotation;
        auto annotationCtx = ctx->test();
        
        if (annotationCtx)
        {
            annotation = visitTest(annotationCtx);
        }
        
        return std::pair{name, annotation};
    }
    
    
    //
    // varargslist: 
    //   (vfpass (',' vfpass)* 
    //     (',' ('*' (vfpdef)? (',' vfpass)* 
    //               (',' ('**' vfpdef (',')?)?)? | '**' vfpdef (',')?)?)?
    //   | '*' (vfpdef)? (',' vfpass)* (',' ('**' vfpdef (',')?)?)?
    //   | '**' vfpdef (',')?
    //
    
    antlrcpp::Any PyParserVisitor::visitVarargslist(Python3Parser::VarargslistContext *ctx)
    {
        // Collect args.
        std::vector<AstArgList::Arg> args;
        args.reserve(ctx->args.size());
        
        for (auto argCtx : ctx->args)
        {
            args.push_back(visitVfpass(argCtx));
        }
        
        // Collect star args.
        std::vector<AstArgList::Arg> sargs;
        sargs.reserve((ctx->sarg0 ? 1 : 0) + ctx->sargs.size());
        
        if (ctx->sarg0)
        {
            std::string name = visitVfpdef(ctx->sarg0);
            sargs.push_back(AstArgList::Arg(name, nullptr, nullptr));
        }
        
        for (auto sargCtx : ctx->sargs)
        {
            sargs.push_back(visitVfpass(sargCtx));
        }
        
        // Collect star star args.
        std::optional<AstArgList::Arg> ssarg;
        if (ctx->ssarg)
        {
            ssarg = {static_cast<std::string>(visitVfpdef(ctx->ssarg)), nullptr, nullptr};
        }
        
        return std::make_shared<AstArgList>(args, sargs, ssarg);
    }
    
    
    //
    // vfpass: NAME ('=' test)?
    //
    
    antlrcpp::Any PyParserVisitor::visitVfpass(Python3Parser::VfpassContext *ctx)
    {
        auto name = ctx->NAME()->getSymbol()->getText();
        auto initValCtx = ctx->test();
        AstExprPtr initVal;
        if (initValCtx)
        {
            initVal = visitTest(initValCtx);
        }
        
        return AstArgList::Arg(name, nullptr, initVal);
    }
    
    
    //
    // vfpdef is just an identifier.
    //
    
    antlrcpp::Any PyParserVisitor::visitVfpdef(Python3Parser::VfpdefContext *ctx)
    {
        return ctx->NAME()->getSymbol()->getText();
    }
    
    
    //
    // Can be a simple statement or a compound statement.
    //
    
    antlrcpp::Any PyParserVisitor::visitStmt(Python3Parser::StmtContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // A simple statement is a series of small statements separated by 
    // semicolons and terminated with a newline.
    //
    
    antlrcpp::Any PyParserVisitor::visitSimple_stmt(Python3Parser::Simple_stmtContext *ctx)
    {
        if (ctx->statements.size() == 1)
        {
            // A single statement on a line.
            return visitSmall_stmt(ctx->statements[0]);
        }
        else
        {
            // A sequence of statements on this line.
            std::vector<AstStmtPtr> statements;
            statements.reserve(ctx->statements.size());
    
            for (auto statement : ctx->statements) 
            {                
                statements.push_back(visitSmall_stmt(statement));
            }    
    
            return std::make_shared<AstStmtBlock>(statements);
        }
    }
    
    
    //
    // A small statement is one of expr_stmt, del_stmt, pass_stmt, 
    // flow_stmt, import_stmt, global_stmt, nonlocal_stmt or assert_stmt.
    //
    
    antlrcpp::Any PyParserVisitor::visitSmall_stmt(Python3Parser::Small_stmtContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // Expression statement: 
    //   testlist_star_expr (annassign | augassign (yield_expr|testlist) |
    //     conv_expr_stmt_clause*)
    //
    
    antlrcpp::Any PyParserVisitor::visitExpr_stmt(Python3Parser::Expr_stmtContext *ctx)
    {
        auto lhs = visitTestlist_star_expr(ctx->lhs);
        if (ctx->ann)
        {
            // Variable annotation.
            std::pair<AstExprPtr, AstExprPtr> ann = visitAnnassign(ctx->ann);
            return std::make_shared<AstExprVariableAnnotation>(lhs, ann.first, ann.second);
        }
        else if (ctx->aug)
        {
            // Augmented assignment (operator assignment).
            BinOp op = visitAugassign(ctx->aug);
            AstExprPtr rhs;
            if (ctx->ay)
            {
                rhs = visitYield_expr(ctx->ay);
            }
            else
            {
                rhs = visitTestlist(ctx->tl);
            }
            
            auto opExpr = std::make_shared<AstExprBinary>(op, lhs, rhs);
            return std::make_shared<AstExprBinary>(BinOp::Assign, lhs, opExpr);
        }
        else if (!ctx->conv.empty())
        {
            // Conventional assignment. Do the assignments in reverse order.
            AstExprPtr rhs = visitConv_expr_stmt_clause(ctx->conv.at(ctx->conv.size()-1));
            for (ssize_t i = static_cast<ssize_t>(ctx->conv.size())-2; i >= 0; i--)
            {
                AstExprPtr ilhs = visitConv_expr_stmt_clause(ctx->conv.at(static_cast<size_t>(i)));
                rhs = std::make_shared<AstExprBinary>(BinOp::Assign, ilhs, rhs);
            }
            
            // Do the last assignment.
            return std::make_shared<AstExprBinary>(BinOp::Assign, lhs, rhs);
        }
        else
        {
            // Just an expression statement with no operation.
            return lhs;
        }
    }
    
    
    //
    // Conventional expression statement clause: '=' (yield_expr|testlist_star_expr)
    //
    
    antlrcpp::Any PyParserVisitor::visitConv_expr_stmt_clause(Python3Parser::Conv_expr_stmt_clauseContext *ctx)
    {
        return visitChildren(ctx);
    }


    //
    // Annotated assignment: ':' test ('=' test)?
    //
    
    antlrcpp::Any PyParserVisitor::visitAnnassign(Python3Parser::AnnassignContext *ctx)
    {
        AstExprPtr ann = visitTest(ctx->ann);
        AstExprPtr val;
        if (ctx->val)
        {
            val = visitTest(ctx->val);
        }
        
        return std::pair<AstExprPtr, AstExprPtr>(ann, val);
    }
    
    
    //
    // testlist_star_clause (',' testlist_star_clause)* (',')?
    //
    
    antlrcpp::Any PyParserVisitor::visitTestlist_star_expr(Python3Parser::Testlist_star_exprContext *ctx)
    {
        auto tscCtx = ctx->testlist_star_clause();
        if (tscCtx.size() == 1)
        {
            // It's just the underlying expression.
            return visitTestlist_star_clause(tscCtx.at(0));
        }
        else
        {
            // Make a test list.
            std::vector<AstExprPtr> testlist;
            testlist.reserve(tscCtx.size());
            
            for (auto tlCtx : tscCtx)
            {
                testlist.push_back(visitTestlist_star_clause(tlCtx));
            }
            
            return std::make_shared<AstExprTestlist>(testlist);
        }
    }
    
    
    //
    // Testlist star clause: test | star_expr
    //
    
    antlrcpp::Any PyParserVisitor::visitTestlist_star_clause(Python3Parser::Testlist_star_clauseContext *ctx)
    {
        return visitChildren(ctx);
    }
    

    //
    // Gets the corresponding operator for an augmented assignment (operator assignment).
    //
    
    antlrcpp::Any PyParserVisitor::visitAugassign(Python3Parser::AugassignContext *ctx)
    {
        switch (ctx->getStart()->getType())
        {
        case Python3Lexer::ADD_ASSIGN:         return BinOp::Add;
        case Python3Lexer::SUB_ASSIGN :        return BinOp::Sub;
        case Python3Lexer::MULT_ASSIGN:        return BinOp::Mult;
        case Python3Lexer::AT_ASSIGN:          return BinOp::At;
        case Python3Lexer::DIV_ASSIGN:         return BinOp::Div;
        case Python3Lexer::MOD_ASSIGN :        return BinOp::Mod;
        case Python3Lexer::AND_ASSIGN:         return BinOp::And;
        case Python3Lexer::OR_ASSIGN:          return BinOp::Or;
        case Python3Lexer::XOR_ASSIGN:         return BinOp::Xor;
        case Python3Lexer::LEFT_SHIFT_ASSIGN : return BinOp::LeftShift;
        case Python3Lexer::RIGHT_SHIFT_ASSIGN: return BinOp::RightShift;
        case Python3Lexer::POWER_ASSIGN:       return BinOp::Power;
        case Python3Lexer::IDIV_ASSIGN:        return BinOp::IDiv;
        default: throw ParserException(std::string("internal error: invalid token index ") + std::to_string(ctx->getStart()->getType()));
        }
    }
    
    
    //
    // Del statements have a single parameter - the value to delete.
    //
    
    antlrcpp::Any PyParserVisitor::visitDel_stmt(Python3Parser::Del_stmtContext *ctx)
    {
        return std::make_shared<AstStmtDel>(visitExprlist(ctx->exprlist()));
    }
    
    
    //
    // Pass statements have no parameters.
    //
    
    antlrcpp::Any PyParserVisitor::visitPass_stmt(Python3Parser::Pass_stmtContext *)
    {
        return std::make_shared<AstStmtPass>();
    }
    
    
    //
    // Flow statements are one of break_stmt, continue_stmt, return_stmt, 
    // raise_stmt or yield_stmt.
    //
    
    antlrcpp::Any PyParserVisitor::visitFlow_stmt(Python3Parser::Flow_stmtContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // Break statements have no parameters.
    //
    
    antlrcpp::Any PyParserVisitor::visitBreak_stmt(Python3Parser::Break_stmtContext *)
    {
        return std::make_shared<AstStmtBreak>();
    }
    
    
    //
    // Continue statements have no parameters.
    //
    
    antlrcpp::Any PyParserVisitor::visitContinue_stmt(Python3Parser::Continue_stmtContext *)
    {
        return std::make_shared<AstStmtContinue>();
    }
    
    
    //
    // Return statements have a single optional parameter.
    //
    
    antlrcpp::Any PyParserVisitor::visitReturn_stmt(Python3Parser::Return_stmtContext *ctx)
    {
        auto tlc = ctx->testlist();
        if (tlc)
        {
            return std::make_shared<AstStmtReturn>(visitTestlist(tlc));
        }
        else
        {
            return std::make_shared<AstStmtReturn>();
        }
    }
    
    
    //
    // Yield statements are just yield_exprs.
    //
    
    antlrcpp::Any PyParserVisitor::visitYield_stmt(Python3Parser::Yield_stmtContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // Raise has two optional tests as arguments.
    //
    
    antlrcpp::Any PyParserVisitor::visitRaise_stmt(Python3Parser::Raise_stmtContext *ctx)
    {
        auto tests = ctx->test();
        if (tests.empty())
        {
            return std::make_shared<AstStmtRaise>();
        }
        else if (tests.size() == 1)
        {
            return std::make_shared<AstStmtRaise>(visitTest(tests[0]));
        }
        else
        {
            return std::make_shared<AstStmtRaise>(visitTest(tests[0]), visitTest(tests[1]));
        }
    }
    
    
    //
    // import_stmt: import_name | import_from
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_stmt(Python3Parser::Import_stmtContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // import_name: 'import' dotted_as_names
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_name(Python3Parser::Import_nameContext *ctx)
    {
        std::vector< std::pair< std::vector<std::string>, std::string > > names = visitDotted_as_names(ctx->dotted_as_names());
        
        return std::make_shared<AstStmtImport>(names);
    }
    
    
    //
    // import_from: ('from' (import_dots* dotted_name | import_dots+)
    //              'import' import_from_names)
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_from(Python3Parser::Import_fromContext *ctx)
    {
        // Count the dots at the start of the dotted name.
        int importDots = 0;
        
        for (auto idCtx : ctx->import_dots())
        {
            int moreDots = visitImport_dots(idCtx);
            importDots += moreDots;
        }
        
        // Get the dotted name.
        std::vector<std::string> dottedName;
        auto dottedNameCtx = ctx->dotted_name();
        if (dottedNameCtx)
        {
            dottedName = static_cast<std::vector<std::string>>(visitDotted_name(dottedNameCtx));
        }
        
        // Get the from names.
        std::vector< std::pair<std::string, std::string> > importFromNames = visitImport_from_names(ctx->import_from_names());
        
        // Make the result.
        return std::make_shared<AstStmtImportFrom>(importDots, dottedName, importFromNames);
    }


    //
    // import_dots: '.' | '...'
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_dots(Python3Parser::Import_dotsContext *ctx)
    {
        if (ctx->getStart()->getType() == Python3Lexer::DOT)
        {
            return 1;   // The number of dots is one.
        }
        else 
        {
            return 3;   // It's an ellipsis - the number of dots is three.
        }
    }

    
    //
    // import_from_names: '*' | '(' import_as_names ')' | import_as_names
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_from_names(Python3Parser::Import_from_namesContext *ctx)
    {
        auto namesCtx = ctx->import_as_names();
        if (namesCtx)
        {
            return visitImport_as_names(namesCtx);
        }
        else 
        {
            return std::vector< std::pair<std::string, std::string> >{ {"*", ""} };
        }
    }
    
    
    //
    // import_as_name: NAME ('as' NAME)?
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_as_name(Python3Parser::Import_as_nameContext *ctx)
    {
        auto importName = ctx->imp->getText();
        std::string asName;
        if (ctx->as)
        {
            asName = ctx->as->getText();
        }
        
        return std::pair<std::string, std::string>(importName, asName);
    }
    
    
    //
    // dotted_as_name: dotted_name ('as' NAME)?
    //
    
    antlrcpp::Any PyParserVisitor::visitDotted_as_name(Python3Parser::Dotted_as_nameContext *ctx)
    {
        auto dottedName = visitDotted_name(ctx->dot);
        std::string asName;
        if (ctx->as)
        {
            asName = ctx->as->getText();
        }
        
        return std::pair<std::vector<std::string>, std::string>(dottedName, asName);
    }
    
    
    //
    // import_as_names: import_as_name (',' import_as_name)* (',')?;
    //
    
    antlrcpp::Any PyParserVisitor::visitImport_as_names(Python3Parser::Import_as_namesContext *ctx)
    {
        std::vector< std::pair<std::string, std::string> > names;
        names.reserve(ctx->names.size());
        
        for (auto &nameCtx : ctx->names)
        {
            std::pair<std::string, std::string> name = visitImport_as_name(nameCtx);
            names.push_back(name);
        }
        
        return std::move(names);
    }
    
    
    //
    // dotted_as_names: dotted_as_name (',' dotted_as_name)*;
    //
    
    antlrcpp::Any PyParserVisitor::visitDotted_as_names(Python3Parser::Dotted_as_namesContext *ctx)
    {
        std::vector< std::pair< std::vector<std::string>, std::string > > names;
        names.reserve(ctx->names.size());
        
        for (auto &nameCtx : ctx->names)
        {
            std::pair< std::vector<std::string>, std::string > name = visitDotted_as_name(nameCtx);
            names.push_back(name);
        }
        
        return std::move(names);
    }
    
    
    //
    // dotted_name: NAME ('.' NAME)*
    //
    
    antlrcpp::Any PyParserVisitor::visitDotted_name(Python3Parser::Dotted_nameContext *ctx)
    {
        std::vector<std::string> allNames;
        allNames.reserve(ctx->names.size());
        
        for (auto &nameToken : ctx->names)
        {
            allNames.push_back(nameToken->getText());
        }
        
        return std::move(allNames);
    }
    
    
    //
    // global_stmt: 'global' NAME (',' NAME)*
    //
    
    antlrcpp::Any PyParserVisitor::visitGlobal_stmt(Python3Parser::Global_stmtContext *ctx)
    {
        auto namesTerminalNodes = ctx->NAME();
        std::vector<std::string> names;
        names.resize(namesTerminalNodes.size());
        
        for (auto ntn : namesTerminalNodes)
        {
            names.push_back(ntn->getSymbol()->getText());
        }
        
        return std::make_shared<AstStmtGlobal>(names);
    }
    
    
    //
    // nonlocal_stmt: 'nonlocal' NAME (',' NAME)*
    //
    
    antlrcpp::Any PyParserVisitor::visitNonlocal_stmt(Python3Parser::Nonlocal_stmtContext *ctx)
    {
        auto namesTerminalNodes = ctx->NAME();
        std::vector<std::string> names;
        names.resize(namesTerminalNodes.size());
        
        for (auto ntn : namesTerminalNodes)
        {
            names.push_back(ntn->getSymbol()->getText());
        }
        
        return std::make_shared<AstStmtNonLocal>(names);
    }
    
    
    //
    // assert_stmt: 'assert' test (',' test)?
    //
    
    antlrcpp::Any PyParserVisitor::visitAssert_stmt(Python3Parser::Assert_stmtContext *ctx)
    {
        AstExprPtr check = visitTest(ctx->check);
        AstExprPtr say;
        
        if (ctx->say)
        {
            say = visitTest(ctx->say);
        }
        
        return std::make_shared<AstStmtAssert>(check, say);
    }
    
    
    //
    // A compound statement is one of if_stmt, while_stmt, for_stmt, try_stmt,
    // with_stmt, funcdef, classdef, decorated or async_stmt.
    //
    
    antlrcpp::Any PyParserVisitor::visitCompound_stmt(Python3Parser::Compound_stmtContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // async_stmt: ASYNC (funcdef | with_stmt | for_stmt)
    //
    
    antlrcpp::Any PyParserVisitor::visitAsync_stmt(Python3Parser::Async_stmtContext *ctx)
    {
        AstStmtPtr stmt;
        
        if (ctx->fd)
        {
            stmt = visitFuncdef(ctx->fd);
        }
        else if (ctx->ws)
        {
            stmt = visitWith_stmt(ctx->ws);
        }
        else
        {
            stmt = visitFor_stmt(ctx->fs);
        }

        stmt->setAsync(true);
        return std::move(stmt);
    }
    
    
    //
    // If statement: 'if' test ':' suite ('elif' test ':' suite)* ('else' ':' suite)?
    //
    
    antlrcpp::Any PyParserVisitor::visitIf_stmt(Python3Parser::If_stmtContext *ctx)
    {
        auto testCtx = ctx->test();
        auto suiteCtx = ctx->suite();
        
        // Gather the tests.
        std::vector<AstExprPtr> tests;
        tests.reserve(testCtx.size());

        for (auto tCtx : testCtx) 
        {                
            tests.push_back(visitTest(tCtx));
        }    

        // Gather the suites.
        std::vector<AstStmtPtr> suites;
        suites.reserve(suiteCtx.size());

        for (auto sCtx : suiteCtx) 
        {                
            suites.push_back(visitSuite(sCtx));
        }
        
        // Get the optional else.
        antlrcpp::Any elseSuite;
        if (ctx->elseSuite)
        {
            elseSuite = visitSuite(ctx->elseSuite);
        }
        
        return std::make_shared<AstStmtIf>(tests, suites, elseSuite);
    }
    
    
    //
    // While statement: 'while' test ':' suite ('else' ':' suite)?
    //
    
    antlrcpp::Any PyParserVisitor::visitWhile_stmt(Python3Parser::While_stmtContext *ctx)
    {
        auto testExpr = visitTest(ctx->test());
        auto suiteCtx = ctx->suite();
        auto bodySuite = visitSuite(ctx->bodySuite);
        antlrcpp::Any elseSuite;
        if (ctx->elseSuite)
        {
            elseSuite = visitSuite(ctx->elseSuite);
        }
        
        return std::make_shared<AstStmtWhile>(testExpr, bodySuite, elseSuite);
    }
    
    
    //
    // For statement: 'for' exprlist 'in' testlist ':' suite ('else' ':' suite)?
    //
    
    antlrcpp::Any PyParserVisitor::visitFor_stmt(Python3Parser::For_stmtContext *ctx)
    {
        auto exprList = visitExprlist(ctx->exprlist());
        auto testList = visitTestlist(ctx->testlist());
        auto suiteCtx = ctx->suite();
        auto bodySuite = visitSuite(ctx->bodySuite);
        AstStmtPtr elseSuite;
        if (ctx->elseSuite)
        {
            elseSuite = visitSuite(ctx->elseSuite);
        }
        
        return std::make_shared<AstStmtFor>(exprList, testList, bodySuite, elseSuite);
    }
    
    
    //
    // Try statement: 'try' ':' suite
    //                ( (except_clause ':' suite)+
    //                  ('else' ':' suite)?
    //                  ('finally' ':' suite)? 
    //                | 'finally' ':' suite) )
    //
    
    antlrcpp::Any PyParserVisitor::visitTry_stmt(Python3Parser::Try_stmtContext *ctx)
    {
        auto bodySuite = visitSuite(ctx->bodySuite);
        if (ctx->finally2Suite)
        {
            // It's the second, shorter form.
            auto finallySuite = visitSuite(ctx->finally2Suite);
            return std::make_shared<AstStmtTry>(bodySuite, finallySuite);
        }
        else
        {
            // It's the first form with except and optional else clauses.
            std::vector<AstTmpExceptEntry> exceptEntries(ctx->exceptClauses.size());

            for (size_t i = 0; i < ctx->exceptClauses.size(); i++)
            {
                std::pair<AstExprPtr, std::string> exceptClause = visitExcept_clause(ctx->exceptClauses[i]);
                exceptEntries[i].test_ = exceptClause.first;
                exceptEntries[i].name_ = exceptClause.second;
            }
            
            for (size_t i = 0; i < ctx->exceptSuites.size(); i++)
            {
                exceptEntries[i].suite_ = visitSuite(ctx->exceptSuites[i]);
            }
            
            // Check for the optional else.
            AstStmtPtr elseSuite;
            if (ctx->elseSuite)
            {
                elseSuite = visitSuite(ctx->elseSuite);
            }
            
            // And the optional 'finally'.
            AstStmtPtr finallySuite;
            if (ctx->finallySuite)
            {
                finallySuite = visitSuite(ctx->finallySuite);
            }
            
            return std::make_shared<AstStmtTry>(bodySuite, exceptEntries, elseSuite, finallySuite);
        }
    }
    
    
    //
    // With statement: 'with' with_item (',' with_item)*  ':' suite
    //
    
    antlrcpp::Any PyParserVisitor::visitWith_stmt(Python3Parser::With_stmtContext *ctx)
    {
        std::vector< std::pair<AstExprPtr, AstExprPtr> > withItems;
        withItems.reserve(ctx->withItems.size());
        
        for (auto wiCtx : ctx->withItems)
        {
            withItems.push_back(visitWith_item(wiCtx));
        }
        
        auto bodySuite = visitSuite(ctx->suite());
        
        return std::make_shared<AstStmtWith>(withItems, bodySuite);
    }
    
    
    //
    // With items: test ('as' expr)?
    //
    
    antlrcpp::Any PyParserVisitor::visitWith_item(Python3Parser::With_itemContext *ctx)
    {
        AstExprPtr testExpr = visitTest(ctx->test());
        auto exprCtx = ctx->expr();
        AstExprPtr exprVal;
        if (exprCtx)
        {
            exprVal = visitExpr(exprCtx);
        }
        
        return std::make_pair(testExpr, exprVal);
    }
    
    
    //
    // Except clause: 'except' (test ('as' NAME)?)?
    //
    
    antlrcpp::Any PyParserVisitor::visitExcept_clause(Python3Parser::Except_clauseContext *ctx)
    {
        auto testCtx = ctx->test();
        auto nameTerminal = ctx->NAME();
        AstExprPtr testExpr;
        std::string name;
        
        if (testCtx)
        {
            testExpr = visitTest(testCtx);
        }
        
        if (nameTerminal)
        {
            name = nameTerminal->getSymbol()->getText();
        }
        
        return std::make_pair(testExpr, name);
    }
    
    
    //
    // Suite: simple_stmt | NEWLINE INDENT stmt+ DEDENT
    //
    
    antlrcpp::Any PyParserVisitor::visitSuite(Python3Parser::SuiteContext *ctx)
    {
        auto simpleStmtCtx = ctx->simple_stmt();
        if (simpleStmtCtx)
        {
            // Just a simple statement.
            return visitSimple_stmt(simpleStmtCtx);
        }
        else
        {
            // It's a compound statement in a block.
            std::vector<AstStmtPtr> block;
            for (auto stmtCtx : ctx->statements)
            {
                block.push_back(visitStmt(stmtCtx));
            }
            
            return std::make_shared<AstStmtBlock>(block);
        }
    }
    
    
    //
    // Test: or_test ('if' or_test 'else' test)? | lambdef
    //
    
    antlrcpp::Any PyParserVisitor::visitTest(Python3Parser::TestContext *ctx)
    {
        auto lambdefCtx = ctx->lambdef();
        if (lambdefCtx)
        {
            // It's a lambda definition.
            return visitLambdef(lambdefCtx);
        }
        else
        {
            // It's an or test with an optional 'if'.
            auto orTest = visitOr_test(ctx->ex);
            if (!ctx->cond)
            {
                // It's just the expression from the or_test.
                return orTest;
            }
            else
            {
                // It's a conditional expression.
                AstExprPtr cond = visitOr_test(ctx->cond);
                AstExprPtr elseTest = visitTest(ctx->test());
                return std::make_shared<AstExprConditional>(cond, orTest, elseTest);
            }
        }
    }
    
    
    //
    // test_nocond: or_test | lambdef_nocond
    //
    
    antlrcpp::Any PyParserVisitor::visitTest_nocond(Python3Parser::Test_nocondContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // lambdef: 'lambda' (varargslist)? ':' test
    //
    
    antlrcpp::Any PyParserVisitor::visitLambdef(Python3Parser::LambdefContext *ctx)
    {
        AstPtr args;
        auto argsCtx = ctx->varargslist();
        if (argsCtx)
        {
            args = visitVarargslist(argsCtx);
        }
        
        AstExprPtr testExpr = visitTest(ctx->test());
        return std::make_shared<AstStmtLambdef>(args, testExpr);
    }
    
    
    //
    // lambdef_nocond: 'lambda' (varargslist)? ':' test_nocond
    //
    
    antlrcpp::Any PyParserVisitor::visitLambdef_nocond(Python3Parser::Lambdef_nocondContext *ctx)
    {
        AstPtr args;
        auto argsCtx = ctx->varargslist();
        if (argsCtx)
        {
            args = visitVarargslist(argsCtx);
        }
        
        AstExprPtr testExpr = visitTest_nocond(ctx->test_nocond());
        return std::make_shared<AstStmtLambdef>(args, testExpr);
    }
    
    
    //
    // or_test: and_test ('or' and_test)*
    //
    
    antlrcpp::Any PyParserVisitor::visitOr_test(Python3Parser::Or_testContext *ctx)
    {
        auto exprs = ctx->and_test();
        auto result = visitAnd_test(exprs.at(0));
        
        for (size_t i = 1; i < exprs.size(); i++)
        {
            auto rhs = visitAnd_test(exprs.at(i));
            result = std::make_shared<AstExprBinary>(BinOp::OrTest, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // and_test: not_test ('and' not_test)*
    //
    
    antlrcpp::Any PyParserVisitor::visitAnd_test(Python3Parser::And_testContext *ctx)
    {
        auto exprs = ctx->not_test();
        auto result = visitNot_test(exprs.at(0));
        
        for (size_t i = 1; i < exprs.size(); i++)
        {
            auto rhs = visitNot_test(exprs.at(i));
            result = std::make_shared<AstExprBinary>(BinOp::AndTest, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // not_test: 'not' not_test | comparison
    //
    
    antlrcpp::Any PyParserVisitor::visitNot_test(Python3Parser::Not_testContext *ctx)
    {
        auto comp = ctx->comparison();
        if (comp)
        {
            return visitComparison(comp);
        }
        else
        {
            auto expr = visitNot_test(ctx->not_test());
            return std::make_shared<AstExprUnary>(AstExprUnary::Op::NotTest, expr);
        }
    }
    
    
    //
    // comparison: expr (comp_op expr)*
    //
    
    antlrcpp::Any PyParserVisitor::visitComparison(Python3Parser::ComparisonContext *ctx)
    {
        auto result = visitExpr(ctx->lhs);
        
        for (size_t i = 0; i < ctx->ex.size(); i++)
        {
            BinOp op = visitComp_op(ctx->co.at(i));
            AstExprPtr rhs = visitExpr(ctx->ex.at(i));
            result = std::make_shared<AstExprBinary>(op, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // '<'|'>'|'=='|'>='|'<='|'<>'|'!='|'in'|'not' 'in'|'is'|'is' 'not';
    //
    
    antlrcpp::Any PyParserVisitor::visitComp_op(Python3Parser::Comp_opContext *ctx)
    {
        switch (ctx->getStart()->getType())
        {
        case Python3Lexer::LESS_THAN:    return BinOp::LessThan;
        case Python3Lexer::GREATER_THAN: return BinOp::GreaterThan;
        case Python3Lexer::EQUALS:       return BinOp::Equals;
        case Python3Lexer::GT_EQ:        return BinOp::GreaterEquals;
        case Python3Lexer::LT_EQ:        return BinOp::LessEquals;
        case Python3Lexer::NOT_EQ_1:     return BinOp::NotEquals;
        case Python3Lexer::NOT_EQ_2:     return BinOp::NotEquals;
        case Python3Lexer::IN:           return BinOp::In;
        case Python3Lexer::NOT:          return BinOp::NotIn;
        case Python3Lexer::IS:
        {
            if (ctx->getStop()->getType() == Python3Lexer::NOT)
                return BinOp::IsNot;
            else
                return BinOp::Is;
        }
        default: throw ParserException(std::string("internal error: invalid comparison token index ") + std::to_string(ctx->getStart()->getType()));
        }
    }
    
    
    //
    // star_expr: '*' expr
    //
    
    antlrcpp::Any PyParserVisitor::visitStar_expr(Python3Parser::Star_exprContext *ctx)
    {
        return std::make_shared<AstExprUnary>(AstExprUnary::Op::Star, visitExpr(ctx->expr()));
    }
    
    
    //
    // star_star_expr: '**' expr
    //
    
    antlrcpp::Any PyParserVisitor::visitStar_star_expr(Python3Parser::Star_star_exprContext *ctx)
    {
        return std::make_shared<AstExprUnary>(AstExprUnary::Op::StarStar, visitExpr(ctx->expr()));
    }
    

    //
    // expr: xor_expr ('|' xor_expr)*
    //
    
    antlrcpp::Any PyParserVisitor::visitExpr(Python3Parser::ExprContext *ctx)
    {
        auto exprs = ctx->xor_expr();
        auto result = visitXor_expr(exprs.at(0));
        
        for (size_t i = 1; i < exprs.size(); i++)
        {
            auto rhs = visitXor_expr(exprs.at(i));
            result = std::make_shared<AstExprBinary>(BinOp::BitwiseOr, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // xor_expr: and_expr ('^' and_expr)*
    //
    
    antlrcpp::Any PyParserVisitor::visitXor_expr(Python3Parser::Xor_exprContext *ctx)
    {
        auto exprs = ctx->and_expr();
        auto result = visitAnd_expr(exprs.at(0));
        
        for (size_t i = 1; i < exprs.size(); i++)
        {
            auto rhs = visitAnd_expr(exprs.at(i));
            result = std::make_shared<AstExprBinary>(BinOp::BitwiseXor, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // and_expr: shift_expr ('&' shift_expr)*
    //
    
    antlrcpp::Any PyParserVisitor::visitAnd_expr(Python3Parser::And_exprContext *ctx)
    {
        auto exprs = ctx->shift_expr();
        auto result = visitShift_expr(exprs.at(0));
        
        for (size_t i = 1; i < exprs.size(); i++)
        {
            auto rhs = visitShift_expr(exprs.at(i));
            result = std::make_shared<AstExprBinary>(BinOp::BitwiseAnd, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // shift_expr: arith_expr (('<<'|'>>') arith_expr)*
    //
    
    antlrcpp::Any PyParserVisitor::visitShift_expr(Python3Parser::Shift_exprContext *ctx)
    {
        auto result = visitArith_expr(ctx->lhs);
        
        for (size_t i = 0; i < ctx->ex.size(); i++)
        {
            auto lexOp = ctx->op.at(i)->getType();
            auto op = (lexOp == Python3Lexer::LEFT_SHIFT) ? BinOp::LeftShift : BinOp::RightShift;
            auto rhs = visitArith_expr(ctx->ex.at(i));
            result = std::make_shared<AstExprBinary>(op, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // arith_expr: term (('+'|'-') term)*
    //
    
    antlrcpp::Any PyParserVisitor::visitArith_expr(Python3Parser::Arith_exprContext *ctx)
    {
        auto result = visitTerm(ctx->lhs);
        
        for (size_t i = 0; i < ctx->ex.size(); i++)
        {
            auto lexOp = ctx->op.at(i)->getType();
            auto op = (lexOp == Python3Lexer::ADD) ? BinOp::Add : BinOp::Sub;
            auto rhs = visitTerm(ctx->ex.at(i));
            result = std::make_shared<AstExprBinary>(op, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // term: lhs=factor (op+=('*'|'@'|'/'|'%'|'//') ex+=factor)*
    //
    
    antlrcpp::Any PyParserVisitor::visitTerm(Python3Parser::TermContext *ctx)
    {
        auto result = visitFactor(ctx->lhs);
        
        for (size_t i = 0; i < ctx->ex.size(); i++)
        {
            BinOp op = BinOp::Mult;
            switch (ctx->op.at(i)->getType())
            {
            case Python3Lexer::STAR: op = BinOp::Mult; break;
            case Python3Lexer::AT:   op = BinOp::At; break;
            case Python3Lexer::DIV:  op = BinOp::Div; break;
            case Python3Lexer::MOD:  op = BinOp::Mod; break;
            case Python3Lexer::IDIV: op = BinOp::IDiv; break;
            }

            auto rhs = visitFactor(ctx->ex.at(i));
            result = std::make_shared<AstExprBinary>(op, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // factor: op=('+'|'-'|'~') factor | power
    //
    
    antlrcpp::Any PyParserVisitor::visitFactor(Python3Parser::FactorContext *ctx)
    {
        if (ctx->op)
        {
            AstExprUnary::Op op = AstExprUnary::Op::Positive;
            switch (ctx->op->getType())
            {
            case Python3Lexer::ADD:    op = AstExprUnary::Op::Positive; break;
            case Python3Lexer::MINUS:  op = AstExprUnary::Op::Negative; break;
            case Python3Lexer::NOT_OP: op = AstExprUnary::Op::BitwiseNot; break;
            }
            
            auto expr = visitFactor(ctx->factor());
            return std::make_shared<AstExprUnary>(op, expr);
        }
        else
        {
            return visitPower(ctx->power());
        }
    }
    
    
    //
    // power: atom_expr ('**' factor)?
    //
    
    antlrcpp::Any PyParserVisitor::visitPower(Python3Parser::PowerContext *ctx)
    {
        auto factor = ctx->factor();
        auto result = visitAtom_expr(ctx->atom_expr());
        
        if (factor)
        {
            auto rhs = visitFactor(factor);
            result = std::make_shared<AstExprBinary>(BinOp::Power, result, rhs);
        }
        
        return result;
    }
    
    
    //
    // atom_expr: (AWAIT)? atom trailer*
    //
    
    antlrcpp::Any PyParserVisitor::visitAtom_expr(Python3Parser::Atom_exprContext *ctx)
    {
        auto awaitCtx = ctx->AWAIT();
        bool await = awaitCtx != nullptr;
        auto result = visitAtom(ctx->atom());
        auto trailers = ctx->trailer();
        
        // Add the trailers.
        for (size_t i = 0; i < trailers.size(); i++)
        {
            AstExprPtr rhs = visitTrailer(trailers.at(i));
            auto rhsUnOp = std::dynamic_pointer_cast<AstExprTmpUnary>(rhs);
            if (rhsUnOp)
            {
                switch (rhsUnOp->getOpType()) 
                {
                case AstExprTmpUnary::Op::TmpFuncCall:
                    // Turn a temp function call into a fully fledged function call.
                    result = std::make_shared<AstExprFuncCall>(result, rhsUnOp->getExpr());
                    break;
                    
                case AstExprTmpUnary::Op::TmpDictLookup:
                    // Turn a temp dictionary lookup into a fully fledged dictionary lookup.
                    result = std::make_shared<AstExprBinary>(BinOp::DictLookup, result, rhsUnOp->getExpr());
                    break;
                
                default:
                    throw ParserException(std::string("internal error: unhandled atom expr type ") + rhsUnOp->getNodeType());
                }
            }
            else
            {
                // Maybe it's a member lookup.
                auto rhsMemberOp = std::dynamic_pointer_cast<AstExprTmpMemberLookup>(rhs);
                if (!rhsMemberOp)
                    throw ParserException(std::string("internal error: invalid atom subexpression "));
                
                // Turn a temp member lookup into a fully fledged member lookup.
                result = std::make_shared<AstExprMemberLookup>(result, rhsMemberOp->getName());
            }
        }
        
        if (await)
        {
            // Add the AWAIT.
            result = std::make_shared<AstExprUnary>(AstExprUnary::Op::Await, result);
        }
        
        return result;
    }
    
    
    //
    // atom: ('(' (yield_expr|testlist_comp)? ')' |
    //        '[' (testlist_comp)? ']' |
    //        '{' (dictorsetmaker)? '}' |
    //        NAME | NUMBER | STRING+ | '...' | 'None' | 'True' | 'False');
    //
    
    antlrcpp::Any PyParserVisitor::visitAtom(Python3Parser::AtomContext *ctx)
    {
        if (ctx->yie)
        {
            // Yield expression.
            return visitYield_expr(ctx->yie);
        }
        else if (ctx->bex)
        {
            // Bracketed expression.
            return visitTestlist_comp(ctx->bex);
        }
        else if (ctx->lst)
        {
            // List of items.
            return visitTestlist_comp(ctx->lst);
        }
        else if (ctx->dic)
        {
            // Directory.
            return visitDictorsetmaker(ctx->dic);
        }
        else if (ctx->nam)
        {
            // Name.
            return std::make_shared<AstExprSymbol>(ctx->nam->getText());
        }
        else if (ctx->num)
        {
            // Number.
            return std::make_shared<AstExprNumber>(ctx->num->getText());
        }
        else if (!ctx->strs.empty())
        {
            // A series of strings.
            //XXX - need to do all the strings, not just the first.
            return std::make_shared<AstExprString>(ctx->strs.at(0)->getText());
        }
        else if (ctx->el)
        {
            // Just a '...'
            return std::make_shared<AstExprEllipsis>();
        }
        else if (ctx->non)
        {
            // 'None' value.
            return std::make_shared<AstExprNone>();
        }
        else if (ctx->tru)
        {
            // 'True' value.
            return std::make_shared<AstExprBoolean>(true);
        }
        else
        {
            // 'False' value.
            return std::make_shared<AstExprBoolean>(false);
        }
    }
    
    
    //
    // testlist_comp: testlist_elem ( comp_for | (',' testlist_elem)* (',')? )
    //
    
    antlrcpp::Any PyParserVisitor::visitTestlist_comp(Python3Parser::Testlist_compContext *ctx)
    {
        if (!ctx->cf && ctx->ex.size() < 2)
        {
            // It's not really much of a list at all. Treat it as a single element.
            return visitTestlist_elem(ctx->ex.at(0));
        }
        else
        {
            // Handle a list with multiple elements.
            std::vector<AstExprPtr> list;
            list.reserve(ctx->ex.size() + (ctx->cf ? 1 : 0));
            
            for (auto e: ctx->ex)
            {
                list.push_back(visitTestlist_elem(e));
            }
            
            if (ctx->cf)
            {
                list.push_back(visitComp_for(ctx->cf));
            }
            
            return std::make_shared<AstExprList>(list);
        }
    }
    
    
    //
    // testlist_elem: test|star_expr
    //
    
    antlrcpp::Any PyParserVisitor::visitTestlist_elem(Python3Parser::Testlist_elemContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // trailer: '(' (arglist)? ')' | '[' subscriptlist ']' | '.' NAME
    //
    
    antlrcpp::Any PyParserVisitor::visitTrailer(Python3Parser::TrailerContext *ctx)
    {
        // emit unary ops for TmpFuncCall, TmpMemberLookup and TmpDictLookup.
        auto nameNode = ctx->NAME();
        if (nameNode)
        {
            return std::make_shared<AstExprTmpMemberLookup>(nameNode->getSymbol()->toString());
        }
        else
        {
            auto subsCtx = ctx->subscriptlist();
            if (subsCtx)
            {
                // Dictionary subscript lookup.
                AstExprPtr subs = visitSubscriptlist(subsCtx);
                return std::make_shared<AstExprTmpUnary>(AstExprTmpUnary::Op::TmpDictLookup, subs);
            }
            else
            {
                // Function call.
                auto argsCtx = ctx->arglist();
                if (argsCtx)
                {
                    auto args = visitArglist(argsCtx);
                    return std::make_shared<AstExprTmpUnary>(AstExprTmpUnary::Op::TmpFuncCall, args);
                }
                else
                {
                    return std::make_shared<AstExprTmpUnary>(AstExprTmpUnary::Op::TmpFuncCall, std::make_shared<AstExprList>());
                }
            }
        }
    }
    
    
    //
    // subscriptlist: subscript (',' subscript)* (',')?
    //
    
    antlrcpp::Any PyParserVisitor::visitSubscriptlist(Python3Parser::SubscriptlistContext *ctx)
    {
        // Multiple subscripts.
        std::vector<AstExprPtr> subs;
        subs.reserve(ctx->sub.size());
        for (auto s: ctx->sub)
        {
            subs.push_back(visitSubscript(s));
        }
            
        return std::make_shared<AstExprList>(subs);
    }
    

    //
    // subscript: test | (test)? ':' (test)? (sliceop)?
    //
    
    antlrcpp::Any PyParserVisitor::visitSubscript(Python3Parser::SubscriptContext *ctx)
    {
        if (ctx->bt)
        {
            // It's just a bare test.
            return visitTest(ctx->bt);
        }
        else
        {
            // It's a slice.
            AstExprPtr left;
            AstExprPtr right;
            AstExprPtr inc;
            
            if (ctx->left)
            {
                left = visitTest(ctx->left);
            }
            
            if (ctx->right)
            {
                right = visitTest(ctx->right);
            }
            
            if (ctx->inc)
            {
                inc = visitSliceop(ctx->inc);
            }

            return std::make_shared<AstExprSlice>(left, right, inc);
        }
    }
    
    
    //
    // sliceop: ':' (test)?
    //
    
    antlrcpp::Any PyParserVisitor::visitSliceop(Python3Parser::SliceopContext *ctx)
    {
        auto testCtx = ctx->test();
        if (testCtx)
        {
            return visitTest(testCtx);
        }
        else
        {
            return AstExprPtr(nullptr);
        }
    }
    
    
    //
    // exprlist: exprlist_item (',' exprlist_item)* (',')?
    //
    
    antlrcpp::Any PyParserVisitor::visitExprlist(Python3Parser::ExprlistContext *ctx)
    {
        // A list of expressions.
        auto items = ctx->exprlist_item();
        std::vector<AstExprPtr> exprlist;
        exprlist.reserve(items.size());
        
        for (auto item: items)
        {
            exprlist.push_back(visitExprlist_item(item));
        }
        
        return std::make_shared<AstExprList>(exprlist);
    }
    
    
    //
    // exprlist_item: expr|star_expr
    //
    
    antlrcpp::Any PyParserVisitor::visitExprlist_item(Python3Parser::Exprlist_itemContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // testlist: test (',' test)* (',')?
    //
    
    antlrcpp::Any PyParserVisitor::visitTestlist(Python3Parser::TestlistContext *ctx)
    {
        auto tests = ctx->test();
        if (tests.size() > 1)
        {
            // A list of expressions.
            std::vector<AstExprPtr> testlist;
            testlist.reserve(tests.size());
            
            for (auto tst: tests)
            {
                testlist.push_back(visitTest(tst));
            }
            
            return std::make_shared<AstExprTestlist>(testlist);
        }
        else
        {
            // Just a single expression.
            return visitTest(tests.at(0));
        }
    }
    
    
    //
    // dictorsetmaker: dictmaker | setmaker
    //
    
    antlrcpp::Any PyParserVisitor::visitDictorsetmaker(Python3Parser::DictorsetmakerContext *ctx)
    {
        return visitChildren(ctx);
    }
    

    //
    // dictmaker: dsst0=dictssterm (cf=comp_for | (',' dssts+=dictssterm)* (',')?)
    //
    
    antlrcpp::Any PyParserVisitor::visitDictmaker(Python3Parser::DictmakerContext *ctx)
    {
        AstExprPtr dsst0 = visitDictssterm(ctx->dsst0);
        
        if (ctx->cf)
        {
            // Comprehension for form.
            AstExprPtr cf = visitComp_for(ctx->cf);
            return std::make_shared<AstExprDict>(dsst0, cf);
        }
        else 
        {
            // Set-of-terms form.
            std::vector<AstExprPtr> dssts;
            dssts.reserve(ctx->dssts.size() + 1);
            dssts.push_back(dsst0);
            
            for (auto dsstsCtx : ctx->dssts)
            {
                dssts.push_back(visitDictssterm(dsstsCtx));
            }
            
            return std::make_shared<AstExprDict>(dssts);
        }
    }

    
    //
    // dictterm:  test ':' test
    //
    
    antlrcpp::Any PyParserVisitor::visitDictterm(Python3Parser::DicttermContext *ctx)
    {
        AstExprPtr t1 = visitTest(ctx->t1);
        AstExprPtr t2 = visitTest(ctx->t2);
        
        return std::make_shared<AstExprDictPair>(t1, t2);
    }

    
    //
    // dictssterm: dictterm | star_star_expr
    //
    
    antlrcpp::Any PyParserVisitor::visitDictssterm(Python3Parser::DictsstermContext *ctx)
    {
        return visitChildren(ctx);
    }

    
    //
    // setmaker:  st0=setterm (cf=comp_for | (',' sts+=setterm)* (',')?)
    //
    
    antlrcpp::Any PyParserVisitor::visitSetmaker(Python3Parser::SetmakerContext *ctx)
    {
        AstExprPtr st0 = visitSetterm(ctx->st0);
        
        if (ctx->cf)
        {
            // Comprehension for form.
            AstExprPtr cf = visitComp_for(ctx->cf);
            return std::make_shared<AstExprSet>(st0, cf);
        }
        else 
        {
            // Set-of-terms form.
            std::vector<AstExprPtr> sts;
            sts.reserve(ctx->sts.size() + 1);
            sts.push_back(st0);
            
            for (auto stsCtx : ctx->sts)
            {
                sts.push_back(visitSetterm(stsCtx));
            }
            
            return std::make_shared<AstExprSet>(sts);
        }
    }

    
    //
    // setterm: test | star_expr
    //
    
    antlrcpp::Any PyParserVisitor::visitSetterm(Python3Parser::SettermContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // classdef: 'class' NAME ('(' (arglist)? ')')? ':' suite;
    //
    
    antlrcpp::Any PyParserVisitor::visitClassdef(Python3Parser::ClassdefContext *ctx)
    {
        auto name = ctx->NAME()->getSymbol()->getText();
        std::vector<AstExprPtr> arglist = visitArglist(ctx->arglist());
        AstStmtPtr body = visitSuite(ctx->suite());

        return std::make_shared<AstStmtClassDef>(name, arglist, body);
    }
    
    
    //
    // arglist: argument (',' argument)*  (',')?
    //
    
    antlrcpp::Any PyParserVisitor::visitArglist(Python3Parser::ArglistContext *ctx)
    {
        std::vector<AstExprPtr> arglist;
        arglist.reserve(ctx->args.size());
        for (auto &argCtx : ctx->args)
        {
            arglist.push_back(visitArgument(argCtx));
        }
        
        return std::move(arglist);
    }
    
    
    //
    // argument: ( test (comp_for)? |
    //    test '=' test |
    //    '**' test |
    //    '*' test )
    //
    
    antlrcpp::Any PyParserVisitor::visitArgument(Python3Parser::ArgumentContext *ctx)
    {
        if (ctx->comp_arg)
        {
            AstExprPtr compArg = visitTest(ctx->comp_arg);
            if (ctx->comp_f)
            {
                // A comprehension for.
                auto compFExpr = visitComp_for(ctx->comp_f);
                auto compF = std::dynamic_pointer_cast<AstTmpCompFor>(static_cast<AstExprPtr>(compFExpr));
                return std::make_shared<AstExprCompFor>(compArg, *compF);
            }
            else
            {
                // Just a normal arg.
                return std::move(compArg);
            }
        }
        else if (ctx->ass_arg)
        {
            // An assignment argument.
            auto assArg = visitTest(ctx->ass_arg);
            auto assExpr = visitTest(ctx->ass_expr);
            return std::make_shared<AstExprBinary>(BinOp::TmpAssignArg, assArg, assExpr);
        }
        else if (ctx->ss_expr)
        {
            // A star star argument.
            return std::make_shared<AstExprTmpUnary>(AstExprTmpUnary::Op::TmpStarStarArg, visitTest(ctx->ss_expr));
        }
        else
        {
            // A star argument.
            return std::make_shared<AstExprTmpUnary>(AstExprTmpUnary::Op::TmpStarArg, visitTest(ctx->s_expr));
        }
    }
    
    
    //
    // comp_iter: comp_for | comp_if
    //
    
    antlrcpp::Any PyParserVisitor::visitComp_iter(Python3Parser::Comp_iterContext *ctx)
    {
        return visitChildren(ctx);
    }
    
    
    //
    // (ASYNC)? 'for' exprlist 'in' or_test (comp_iter)?
    //
    
    antlrcpp::Any PyParserVisitor::visitComp_for(Python3Parser::Comp_forContext *ctx)
    {
        bool async = ctx->ASYNC() != nullptr;
        auto exprlist = visitExprlist(ctx->exprlist());
        auto orTest = visitOr_test(ctx->or_test());
        auto compIterCtx = ctx->comp_iter();
        AstExprPtr compIter;
        
        if (compIterCtx)
        {
            compIter = visitComp_iter(compIterCtx);
        }

        return std::make_shared<AstTmpCompFor>(async, exprlist, orTest, compIter);
    }
    
    
    //
    // 'if' test_nocond (comp_iter)?
    //
    
    antlrcpp::Any PyParserVisitor::visitComp_if(Python3Parser::Comp_ifContext *ctx)
    {
        auto cond = visitTest_nocond(ctx->test_nocond());

        AstExprPtr iter;
        auto iterCtx = ctx->comp_iter();
        if (iterCtx)
        {
            iter = visitComp_iter(iterCtx);
        }
        
        return std::make_shared<AstTmpCompIf>(cond, iter);
    }
    
    
    //
    // encoding_decl: NAME
    //
    
    antlrcpp::Any PyParserVisitor::visitEncoding_decl(Python3Parser::Encoding_declContext *ctx)
    {
        return ctx->NAME()->getSymbol()->getText();
    }
    
    
    //
    // yield_expr: 'yield' ('from' frm=test | args=testlist)?
    //
    
    antlrcpp::Any PyParserVisitor::visitYield_expr(Python3Parser::Yield_exprContext *ctx)
    {
        if (ctx->frm)
        {
            auto from = visitTest(ctx->frm);
            return std::make_shared<AstStmtYield>(from, true);
        }
        else
        {
            auto args = visitTestlist(ctx->args);
            return std::make_shared<AstStmtYield>(args, false);
        }
    }

} // namespace fastpython.
