//
// The Ast class represents an Abstract Syntax Tree - the result of parsing
// a Python file.
//

#ifndef AST_H
#define AST_H

#include <memory>
#include <vector>
#include <algorithm>
#include <optional>
#include <any>
#include <antlr4-runtime.h>

#include "op.h"


namespace fastpython {

    // Forward declarations.
    class Ast;
    class AstStmt;
    class AstExpr;
    class AstDecorator;
    class AstVisitor;
    
    
    // Types.
    typedef std::shared_ptr<Ast> AstPtr;
    typedef std::shared_ptr<AstStmt> AstStmtPtr;
    typedef std::shared_ptr<AstExpr> AstExprPtr;
    typedef std::shared_ptr<AstDecorator> AstDecoratorPtr;
    

    //
    // Abstract Syntax Tree.
    //
    
    class Ast
    {
    public:
        Ast() {}
        
        virtual ~Ast();
        virtual const char *getNodeType() const { return "ast"; }
        
        // For visitors using AstVisitor.
        virtual std::any accept(AstVisitor &v) = 0;
    };
    
    
    //
    // Statement.    
    //
    
    class AstStmt : public Ast
    {
    public:
        AstStmt() {}
        
        virtual ~AstStmt() override;
        virtual void setAsync(bool) {}
        virtual const char *getNodeType() const override { return "stmt"; }
        
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // A block of statements.
    //
    
    class AstStmtBlock : public AstStmt
    {
    protected:
        std::vector<AstStmtPtr> children_;

    public:
        AstStmtBlock() {}
        AstStmtBlock(const std::vector<AstStmtPtr> &children) : children_(children) {}
        virtual ~AstStmtBlock() override;

        const char *getNodeType() const override { return "block"; }
        std::any accept(AstVisitor &v) override;
        
        void appendChild(const AstStmtPtr child) { children_.push_back(child); }
        const std::vector<AstStmtPtr> &getChildren() const { return children_; }
    };
    
    
    //
    // Function definition statement.
    //
    
    class AstStmtFuncDef : public AstStmt
    {
    protected:
        std::vector<AstDecoratorPtr> decorators_;
        bool async_;
        std::string name_;
        AstExprPtr params_;
        AstExprPtr returns_;
        AstStmtPtr body_;
        
    public:
        explicit AstStmtFuncDef(AstStmtPtr body)
            : async_(false),
              body_(body)
        {}
        
        explicit AstStmtFuncDef(bool async, const std::string &name, AstExprPtr params, AstExprPtr returns, AstStmtPtr body)
            : async_(async),
              name_(name),
              params_(params),
              returns_(returns),
              body_(body)
        {}
        
        virtual ~AstStmtFuncDef() override;
        
        void setAsync(bool async) override { async_ = async; }
        void setDecorators(const std::vector<AstDecoratorPtr> &decorators) { decorators_ = decorators; }
        
        const char *getNodeType() const override { return "funcdef"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // 'if' statement.
    //
    
    class AstStmtIf : public AstStmt
    {
    protected:
        std::vector<AstExprPtr> tests_;
        std::vector<AstStmtPtr> suites_;
        AstStmtPtr elseSuite_;
        
    public:
        AstStmtIf(const std::vector<AstExprPtr> &tests, const std::vector<AstStmtPtr> &suites, AstStmtPtr elseSuite) 
            : tests_(tests), suites_(suites), elseSuite_(elseSuite)
        {}
        virtual ~AstStmtIf() override;
        
        const char *getNodeType() const override { return "if"; }
        std::any accept(AstVisitor &v) override;
        
        std::vector<AstExprPtr> &getTests() { return tests_; }
        std::vector<AstStmtPtr> &getSuites() { return suites_; }
        AstStmtPtr getElseSuite() { return elseSuite_; }
    };
    
    
    //
    // 'while' statement.
    //
    
    class AstStmtWhile : public AstStmt
    {
    protected:
        AstExprPtr test_;
        AstStmtPtr thenSuite_;
        AstStmtPtr elseSuite_;

    public:        
        AstStmtWhile(AstExprPtr test, AstStmtPtr thenSuite, AstStmtPtr elseSuite) 
            : test_(test), thenSuite_(thenSuite), elseSuite_(elseSuite) 
        {}
        virtual ~AstStmtWhile() override;
        
        const char *getNodeType() const override { return "while"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // for_stmt: 'for' exprlist 'in' testlist ':' bodySuite=suite ('else' ':' elseSuite=suite)?
    //
    
    class AstStmtFor : public AstStmt
    {
    protected:
        AstExprPtr exprList_;
        AstExprPtr testList_;
        AstStmtPtr bodySuite_;
        AstStmtPtr elseSuite_;
        bool async_;
        
    public:
        AstStmtFor(AstExprPtr exprList,
                   AstExprPtr testList,
                   AstStmtPtr bodySuite,
                   AstStmtPtr elseSuite)
            : exprList_(exprList),
              testList_(testList),
              bodySuite_(bodySuite),
              elseSuite_(elseSuite),
              async_(false)
        {}
        
        virtual ~AstStmtFor() override;
        
        void setAsync(bool async) override { async_ = async; }
        
        const char *getNodeType() const override { return "for"; }
        std::any accept(AstVisitor &v) override;
    };
    

    //
    // 'del' statement.
    //
    
    class AstStmtDel : public AstStmt
    {
    protected:
        AstStmtPtr expr_;
        
    public:
        AstStmtDel() {}
        AstStmtDel(AstStmtPtr expr) : expr_(expr) {}
        virtual ~AstStmtDel() override;
        
        const char *getNodeType() const override { return "del"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // 'pass' statement.
    //
    
    class AstStmtPass : public AstStmt
    {
    public:
        AstStmtPass() {}
        virtual ~AstStmtPass() override;
        
        const char *getNodeType() const override { return "pass"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // 'break' statement.
    //
    
    class AstStmtBreak : public AstStmt
    {
    public:
        AstStmtBreak() {}
        virtual ~AstStmtBreak() override;
        
        const char *getNodeType() const override { return "break"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // 'continue' statement.
    //
    
    class AstStmtContinue : public AstStmt
    {
    public:
        AstStmtContinue() {}
        virtual ~AstStmtContinue() override;
        
        const char *getNodeType() const override { return "continue"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // 'return' statement.
    //
    
    class AstStmtReturn : public AstStmt
    {
    protected:
        AstStmtPtr expr_;
        
    public:
        AstStmtReturn() {}
        AstStmtReturn(AstStmtPtr expr) : expr_(expr) {}
        virtual ~AstStmtReturn() override;
        
        const char *getNodeType() const override { return "return"; }
        std::any accept(AstVisitor &v) override;
    };

    
    //
    // 'yield' statement.
    //
    
    class AstStmtYield : public AstStmt
    {
    protected:
        AstExprPtr args_;
        bool fromMode_;
        
    public:
        explicit AstStmtYield(AstExprPtr args, bool fromMode) : args_(args), fromMode_(fromMode) {}
        virtual ~AstStmtYield() override;
        
        const char *getNodeType() const override { return "yield"; }
        std::any accept(AstVisitor &v) override;
    };    
    
    
    //
    // 'raise' statement.
    //
    
    class AstStmtRaise : public AstStmt
    {
    protected:
        AstStmtPtr what_;
        AstStmtPtr from_;
        
    public:
        explicit AstStmtRaise(AstStmtPtr what = nullptr, AstStmtPtr from = nullptr) : what_(what), from_(from) {}
        virtual ~AstStmtRaise() override;
        
        const char *getNodeType() const override { return "raise"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    class AstTmpExceptEntry
    {
    public:
        AstExprPtr test_;
        std::string name_;
        AstStmtPtr suite_;
    };
    
    
    //
    // 'try' statement.
    //
    
    class AstStmtTry : public AstStmt
    {
    protected:
        AstStmtPtr bodySuite_;
        std::vector<AstTmpExceptEntry> exceptEntries_;
        AstStmtPtr elseSuite_;
        AstStmtPtr finallySuite_;
        
    public:
        explicit AstStmtTry(AstStmtPtr bodySuite, const std::vector<AstTmpExceptEntry> &exceptEntries, AstStmtPtr elseSuite, AstStmtPtr finallySuite) 
            : bodySuite_(bodySuite), 
              exceptEntries_(exceptEntries),
              elseSuite_(elseSuite),
              finallySuite_(finallySuite)
        {}
        
        explicit AstStmtTry(AstStmtPtr bodySuite, AstStmtPtr finallySuite) 
            : bodySuite_(bodySuite), 
              finallySuite_(finallySuite)
        {}
        
        virtual ~AstStmtTry() override;
        
        const char *getNodeType() const override { return "try"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // 'with' statement.
    //
    
    class AstStmtWith : public AstStmt
    {
    protected:
        std::vector< std::pair<AstExprPtr, AstExprPtr> > withItems_;
        AstStmtPtr bodySuite_;
        bool async_;
        
    public:
        explicit AstStmtWith(const std::vector< std::pair<AstExprPtr, AstExprPtr> > &withItems, AstStmtPtr bodySuite) 
            : withItems_(withItems), 
              bodySuite_(bodySuite)
        {}
        
        virtual ~AstStmtWith() override;
        
        void setAsync(bool async) override { async_ = async; }
        
        const char *getNodeType() const override { return "with"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Lambda definition.
    //
    
    class AstStmtLambdef : public AstStmt
    {
    protected:
        AstPtr args_;
        AstExprPtr expr_;
        
    public:
        explicit AstStmtLambdef(AstPtr args, AstExprPtr expr) : args_(args), expr_(expr) {}
        virtual ~AstStmtLambdef() override;
        
        const char *getNodeType() const override { return "lambdef"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Expression superclass.
    //
    
    class AstExpr : public Ast
    {
    public:
        AstExpr() {}
        virtual ~AstExpr() override;
        
        const char *getNodeType() const override { return "expr"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprUnary : public AstExpr
    {
    public:
        typedef UnOp Op;
        
    protected:
        Op type_;
        AstExprPtr expr_;
    
    public:
        explicit AstExprUnary(Op typ, std::shared_ptr<AstExpr> expr) 
            : type_(typ), expr_(expr) 
        {}
        
        virtual ~AstExprUnary() override;
        
        Op getOpType() const { return type_; }
        AstExprPtr getExpr() const { return expr_; }
        
        const char *getNodeType() const override;
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprBinary : public AstExpr
    {
    protected:
        BinOp type_;
        AstExprPtr left_;
        AstExprPtr right_;
    
    public:
        explicit AstExprBinary(BinOp typ, std::shared_ptr<AstExpr> left, std::shared_ptr<AstExpr> right) 
            : type_(typ), left_(left), right_(right) 
        {}
        
        virtual ~AstExprBinary() override;
        
        BinOp getOpType() const { return type_; }
        AstExprPtr getLeft() const { return left_; }
        AstExprPtr getRight() const { return right_; }
        
        const char *getNodeType() const override;
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprFuncCall : public AstExpr
    {
    protected:
        AstExprPtr func_;
        AstExprPtr args_;
    
    public:
        explicit AstExprFuncCall(std::shared_ptr<AstExpr> func, std::shared_ptr<AstExpr> args) 
            : func_(func), args_(args) 
        {}
        
        virtual ~AstExprFuncCall() override;
        
        const char *getNodeType() const override { return "func_call"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprConditional : public AstExpr
    {
    protected:
        AstExprPtr condition_;
        AstExprPtr thenExpr_;
        AstExprPtr elseExpr_;
        
    public:
        explicit AstExprConditional(AstExprPtr condition, AstExprPtr thenExpr, AstExprPtr elseExpr)
            : condition_(condition), thenExpr_(thenExpr), elseExpr_(elseExpr)
        {}
        
        virtual ~AstExprConditional() override;
        
        const char *getNodeType() const override { return "cond_expr"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprVariableAnnotation : public AstExpr
    {
    protected:
        AstExprPtr var_;
        AstExprPtr annotation_;
        AstExprPtr value_;
        
    public:
        explicit AstExprVariableAnnotation(AstExprPtr var, AstExprPtr annotation, AstExprPtr value)
            : var_(var), annotation_(annotation), value_(value)
        {}
        
        virtual ~AstExprVariableAnnotation() override;
        
        const char *getNodeType() const override { return "var_annotation"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprSlice : public AstExpr
    {
    protected:
        AstExprPtr left_;
        AstExprPtr right_;
        AstExprPtr inc_;
        
    public:
        explicit AstExprSlice(AstExprPtr left, AstExprPtr right, AstExprPtr inc)
            : left_(left), right_(right), inc_(inc)
        {}
        
        virtual ~AstExprSlice() override;
        
        const char *getNodeType() const override { return "slice"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprList : public AstExpr
    {
    protected:
        std::vector<AstExprPtr> list_;
        
    public:
        explicit AstExprList() {}
        explicit AstExprList(const std::vector<AstExprPtr> &list) : list_(list) {}
        virtual ~AstExprList() override;
        
        const char *getNodeType() const override { return "exprlist"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprTestlist : public AstExpr
    {
    protected:
        std::vector<AstExprPtr> list_;
        
    public:
        explicit AstExprTestlist(const std::vector<AstExprPtr> &list) : list_(list) {}
        virtual ~AstExprTestlist() override;
        
        const char *getNodeType() const override { return "testlist"; }
        std::any accept(AstVisitor &v) override;
    };


    //
    // Class definition statement.
    //
    
    class AstStmtClassDef : public AstStmt
    {
    protected:
        std::vector<AstDecoratorPtr> decorators_;
        std::string name_;
        std::vector<AstExprPtr> args_;
        AstStmtPtr body_;
        
    public:
        explicit AstStmtClassDef(const std::string &name, std::vector<AstExprPtr> &args, AstStmtPtr body)
            : name_(name),
              args_(args),
              body_(body)
        {}
        
        virtual ~AstStmtClassDef() override;
        
        void setDecorators(const std::vector<AstDecoratorPtr> &decorators) { decorators_ = decorators; }
        
        const char *getNodeType() const override { return "classdef"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Import statement.
    //
    
    class AstStmtImport : public AstStmt
    {
    protected:
        std::vector< std::pair< std::vector<std::string>, std::string > > names_;
        
    public:
        explicit AstStmtImport(std::vector< std::pair< std::vector<std::string>, std::string > > &names)
            : names_(names)
        {}
        
        virtual ~AstStmtImport() override;
        
        const char *getNodeType() const override { return "import"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // "from...import" statement.
    //
    
    class AstStmtImportFrom : public AstStmt
    {
    protected:
        int importDots_;
        std::vector<std::string> dottedName_;
        std::vector< std::pair<std::string, std::string> > importFromNames_;
        
    public:
        explicit AstStmtImportFrom(int importDots, std::vector<std::string> &dottedName, std::vector< std::pair<std::string, std::string> > &importFromNames)
            : importDots_(importDots), dottedName_(dottedName), importFromNames_(importFromNames)
        {}
        
        virtual ~AstStmtImportFrom() override;
        
        const char *getNodeType() const override { return "import_from"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Global statement.
    //
    
    class AstStmtGlobal : public AstStmt
    {
    protected:
        std::vector<std::string> names_;
        
    public:
        explicit AstStmtGlobal(std::vector<std::string> &names)
            : names_(names)
        {}
        
        virtual ~AstStmtGlobal() override;
        
        const char *getNodeType() const override { return "global"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Non-local statement.
    //
    
    class AstStmtNonLocal : public AstStmt
    {
    protected:
        std::vector<std::string> names_;
        
    public:
        explicit AstStmtNonLocal(std::vector<std::string> &names)
            : names_(names)
        {}
        
        virtual ~AstStmtNonLocal() override;
        
        const char *getNodeType() const override { return "non_local"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Assert statement.
    //
    
    class AstStmtAssert : public AstStmt
    {
    protected:
        AstExprPtr check_;
        AstExprPtr say_;
        
    public:
        explicit AstStmtAssert(AstExprPtr check, AstExprPtr say)
            : check_(check), say_(say)
        {}
        
        virtual ~AstStmtAssert() override;
        
        const char *getNodeType() const override { return "assert"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Argument list.
    //
    
    class AstArgList : public Ast
    {
    public:
        // Tuple with: name, type annotation, initialiser.
        typedef std::tuple<std::string, AstExprPtr, AstExprPtr> Arg;
        
    protected:
        std::vector<Arg> args_;
        std::vector<Arg> sargs_;
        std::optional<Arg> ssarg_;
        
    public:
        explicit AstArgList(const std::vector<Arg> &args, const std::vector<Arg> &sargs, const std::optional<Arg> &ssarg) :
            args_(args), sargs_(sargs), ssarg_(ssarg)
        {}
        
        virtual ~AstArgList() override;
        
        const char *getNodeType() const override { return "arglist"; }
        std::any accept(AstVisitor &v) override;
    };

    
    class AstExprSymbol : public AstExpr
    {
    protected:
        std::string name_;
        
    public:
        explicit AstExprSymbol(const std::string &name) : name_(name) {}
        virtual ~AstExprSymbol() override;
        
        const char *getNodeType() const override { return "name"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprValue : public AstExpr
    {
    public:
        enum class Typ
        {
            None,
            Boolean,
            Number,
            String
        };
        
    public:
        explicit AstExprValue() {}
        virtual ~AstExprValue() override;
        
        virtual Typ getType() = 0;
        
        const char *getNodeType() const override { return "value"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprNone : public AstExprValue
    {
    public:
        explicit AstExprNone() {}
        virtual ~AstExprNone() override;
        
        Typ getType() override { return Typ::None; }
        
        const char *getNodeType() const override { return "none"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprBoolean : public AstExprValue
    {
    protected:
        bool value_;
        
    public:
        explicit AstExprBoolean(bool value) : value_(value) {}
        virtual ~AstExprBoolean() override;
        
        Typ getType() override { return Typ::Boolean; }
        
        const char *getNodeType() const override { return "boolean"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprNumber : public AstExprValue
    {
    protected:
        std::string number_;
        
    public:
        explicit AstExprNumber(const std::string &number) : number_(number) {}
        virtual ~AstExprNumber() override;
        
        Typ getType() override { return Typ::Number; }
        
        const char *getNodeType() const override { return "number"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprString : public AstExprValue
    {
    protected:
        std::string str_;
        
    public:
        explicit AstExprString(const std::string &str) : str_(str) {}
        virtual ~AstExprString() override;
        
        Typ getType() override { return Typ::String; }
        
        const char *getNodeType() const override { return "string"; }
        std::any accept(AstVisitor &v) override;
    };
    
    class AstExprEllipsis : public AstExpr
    {
    public:
        explicit AstExprEllipsis() {}
        virtual ~AstExprEllipsis() override;
        
        const char *getNodeType() const override { return "ellipsis"; }
        std::any accept(AstVisitor &v) override;
    };


    //
    // A temporary used to pass a block back to a parent.
    //
    
    class AstTmpIncludedBlock : public AstStmtBlock
    {
    public:
        AstTmpIncludedBlock() {}
        AstTmpIncludedBlock(const std::vector<AstStmtPtr> &children) : AstStmtBlock(children) {}
        virtual ~AstTmpIncludedBlock() override;

        const char *getNodeType() const override { return "tmp_included_block"; }
    };
    
    
    //
    // A temporary construct for list comprehension 'for'.
    //
    
    class AstTmpCompFor : public AstExpr
    {
    protected:
        bool async_;
        AstExprPtr expr_;
        AstExprPtr in_;
        AstExprPtr iter_;
        
    public:
        explicit AstTmpCompFor(bool async, AstExprPtr expr, AstExprPtr in, AstExprPtr iter)
            : async_(async), expr_(expr), in_(in), iter_(iter)
        {}
        
        virtual ~AstTmpCompFor() override;
        
        bool getAsync() const { return async_; }
        AstExprPtr getExpr() const { return expr_; }
        AstExprPtr getIn() const { return in_; }
        AstExprPtr getIter() const { return iter_; }
        
        const char *getNodeType() const override { return "tmp_comp_for"; }
    };
    
    
    //
    // List comprehension 'for'.
    //
    
    class AstExprCompFor : public AstExpr
    {
    protected:
        AstExprPtr lhs_;
        bool async_;
        AstExprPtr target_;
        AstExprPtr in_;
        AstExprPtr iter_;
        
    public:
        explicit AstExprCompFor(AstExprPtr lhs, bool async, AstExprPtr target, AstExprPtr in, AstExprPtr iter)
            : lhs_(lhs), async_(async), target_(target), in_(in), iter_(iter)
        {}
        
        explicit AstExprCompFor(AstExprPtr lhs, AstTmpCompFor &rhs)
            : lhs_(lhs), async_(rhs.getAsync()), target_(rhs.getExpr()), in_(rhs.getIn()), iter_(rhs.getIter())
        {}
        
        virtual ~AstExprCompFor() override;
        
        const char *getNodeType() const override { return "comp_for"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // A temporary construct for list comprehension 'if'.
    //
    
    class AstTmpCompIf : public AstExpr
    {
    protected:
        AstExprPtr cond_;
        AstExprPtr iter_;
        
    public:
        explicit AstTmpCompIf(AstExprPtr cond, AstExprPtr iter)
            : cond_(cond), iter_(iter)
        {}
        
        virtual ~AstTmpCompIf() override;
        
        AstExprPtr getCond() const { return cond_; }
        AstExprPtr getIter() const { return iter_; }
        
        const char *getNodeType() const override { return "tmp_comp_if"; }
    };
    
    
    //
    // List comprehension 'if'.
    //
    
    class AstExprCompIf : public AstExpr
    {
    protected:
        AstExprPtr lhs_;
        AstExprPtr cond_;
        AstExprPtr iter_;
        
    public:
        explicit AstExprCompIf(AstExprPtr lhs, AstExprPtr cond, AstExprPtr iter)
            : lhs_(lhs), cond_(cond), iter_(iter)
        {}
        
        explicit AstExprCompIf(AstExprPtr lhs, AstTmpCompIf &rhs)
            : lhs_(lhs), cond_(rhs.getCond()), iter_(rhs.getIter())
        {}
        
        virtual ~AstExprCompIf() override;
        
        const char *getNodeType() const override { return "comp_if"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // A temporary construct for member lookup.
    //
    
    class AstExprTmpMemberLookup : public AstExpr
    {
    protected:
        std::string name_;
        
    public:
        AstExprTmpMemberLookup(const std::string &name) : name_(name) {}
        
        virtual ~AstExprTmpMemberLookup() override;
        
        const std::string &getName() const { return name_; }
        
        const char *getNodeType() const override { return "tmp_member_lookup"; }
    };

    
    //
    // Member lookup.
    //
    
    class AstExprMemberLookup : public AstExpr
    {
    protected:
        AstExprPtr left_;
        std::string name_;
        
    public:
        AstExprMemberLookup(AstExprPtr left, const std::string &name) : left_(left), name_(name) {}
        
        virtual ~AstExprMemberLookup() override;
        
        const char *getNodeType() const override { return "member_lookup"; }
        std::any accept(AstVisitor &v) override;
    };

    
    //
    // A temporary unary operation we'll convert into some other operation.
    //
    
    class AstExprTmpUnary : public AstExpr
    {
    public:
        enum class Op
        {
            TmpStarArg,
            TmpStarStarArg,
            TmpFuncCall,
            TmpDictLookup
        };
        
    protected:
        Op type_;
        AstExprPtr expr_;
    
    public:
        explicit AstExprTmpUnary(Op typ, std::shared_ptr<AstExpr> expr) 
            : type_(typ), expr_(expr) 
        {}
        
        virtual ~AstExprTmpUnary() override;
        
        Op getOpType() const { return type_; }
        AstExprPtr getExpr() const { return expr_; }
        
        const char *getNodeType() const override;
    };
    
    
    //
    // Dict expression.
    //

    class AstExprDict : public AstExpr
    {
    private:
        // Dict of pairs form.
        std::vector<AstExprPtr> pairs_;

        // Comprehension for form.
        AstExprPtr first_;
        AstExprPtr compFor_;
        
    public:
        explicit AstExprDict(std::vector<AstExprPtr> &pairs)
            : pairs_(pairs)
        {}
        
        explicit AstExprDict(AstExprPtr &first, AstExprPtr &compFor)
            : first_(first), compFor_(compFor)
        {}
        
        virtual ~AstExprDict() override;
        
        const char *getNodeType() const override { return "dict"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // A key/value pair in a dictionary row.
    //
    
    class AstExprDictPair : public AstExpr
    {
    private:
        AstExprPtr key_;
        AstExprPtr value_;
        
    public:
        explicit AstExprDictPair(AstExprPtr &key, AstExprPtr &value)
            : key_(key), value_(value)
        {}
        
        virtual ~AstExprDictPair() override;
        
        const char *getNodeType() const override { return "dict_pair"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Set expression.
    //

    class AstExprSet : public AstExpr
    {
    private:
        // Set of items form.
        std::vector<AstExprPtr> items_;

        // Comprehension for form.
        AstExprPtr first_;
        AstExprPtr compFor_;
        
    public:
        explicit AstExprSet(std::vector<AstExprPtr> &items)
            : items_(items)
        {}
        
        explicit AstExprSet(AstExprPtr &first, AstExprPtr &compFor)
            : first_(first), compFor_(compFor)
        {}
        
        virtual ~AstExprSet() override;
        
        const char *getNodeType() const override { return "set"; }
        std::any accept(AstVisitor &v) override;
    };
    
    
    //
    // Decorator for class def and func def.
    //

    class AstDecorator : public Ast
    {
    private:
        std::vector<std::string> dottedNames_;
        std::shared_ptr<AstArgList> argList_;
        
    public:
        explicit AstDecorator(const std::vector<std::string> &dottedNames, std::shared_ptr<AstArgList> argList)
            : dottedNames_(dottedNames), argList_(argList)
        {}
        
        virtual ~AstDecorator() override;
        
        const char *getNodeType() const override { return "decorator"; }
        std::any accept(AstVisitor &v) override;
    };
}

#endif // AST_H
