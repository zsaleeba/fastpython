#ifndef PARSEREXCEPTION_H
#define PARSEREXCEPTION_H

#include <exception>
#include <string>


namespace fastpython {


    class ParserException : public std::exception
    {
        size_t      line_;
        size_t      column_;
        std::string message_;
        
    public:
        ParserException(const std::string &message);
        ParserException(size_t line, size_t column, const std::string &message);
        
        virtual ~ParserException() override;
        
        virtual const char *what() const noexcept override
        {
            return message_.c_str();
        }
    };

    
}


#endif // PARSEREXCEPTION_H
