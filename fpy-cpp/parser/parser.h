#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <memory>

#include "ast.h"


namespace fastpython {

    class Parser
    {
        std::string filename_;
        
    public:
        Parser(const std::string &filename);
        
        std::shared_ptr<Ast> parse();
    };

}

#endif // PARSER_H
