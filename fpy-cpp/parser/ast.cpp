#include "ast.h"
#include "astvisitor.h"


namespace fastpython {


Ast::~Ast()
{
}

AstStmt::~AstStmt()
{
}

AstArgList::~AstArgList()
{
}

std::any AstArgList::accept(AstVisitor &v) 
{
    return v.visit(*this); 
}

AstStmtBlock::~AstStmtBlock()
{
}

std::any AstStmt::accept(AstVisitor &v) 
{
    return v.visit(*this); 
}

std::any AstStmtBlock::accept(AstVisitor &v) 
{
    return v.visit(*this); 
}

AstTmpIncludedBlock::~AstTmpIncludedBlock()
{
}

AstStmtIf::~AstStmtIf()
{
}

std::any AstStmtIf::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtWhile::~AstStmtWhile()
{
}

std::any AstStmtWhile::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtFuncDef::~AstStmtFuncDef()
{
}

std::any AstStmtFuncDef::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtClassDef::~AstStmtClassDef()
{
}

std::any AstStmtClassDef::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtFor::~AstStmtFor()
{
}

std::any AstStmtFor::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtDel::~AstStmtDel()
{
}

std::any AstStmtDel::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtPass::~AstStmtPass()
{
}

std::any AstStmtPass::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtBreak::~AstStmtBreak()
{
}

std::any AstStmtBreak::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtContinue::~AstStmtContinue()
{
}

std::any AstStmtContinue::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtReturn::~AstStmtReturn()
{
}

std::any AstStmtReturn::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtYield::~AstStmtYield()
{
}

std::any AstStmtYield::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtRaise::~AstStmtRaise()
{
}

std::any AstStmtRaise::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtTry::~AstStmtTry()
{
}

std::any AstStmtTry::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtWith::~AstStmtWith()
{
}

std::any AstStmtWith::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtLambdef::~AstStmtLambdef()
{
}

std::any AstStmtLambdef::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtImport::~AstStmtImport()
{
}

std::any AstStmtImport::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtImportFrom::~AstStmtImportFrom()
{
}

std::any AstStmtImportFrom::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtGlobal::~AstStmtGlobal()
{
}

std::any AstStmtGlobal::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtNonLocal::~AstStmtNonLocal()
{
}

std::any AstStmtNonLocal::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstStmtAssert::~AstStmtAssert()
{
}

std::any AstStmtAssert::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExpr::~AstExpr()
{
}

std::any AstExpr::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprBinary::~AstExprBinary()
{
}

const char *AstExprBinary::getNodeType() const
{
    switch (type_)
    {
    case BinOp::Add:           return "add";
    case BinOp::Sub:           return "sub";
    case BinOp::Mult:          return "mult";
    case BinOp::At:            return "at";
    case BinOp::Div:           return "div";
    case BinOp::Mod:           return "mod";
    case BinOp::Power:         return "power";
    case BinOp::IDiv:          return "idiv";
    case BinOp::Or:            return "or";
    case BinOp::And:           return "and";
    case BinOp::Xor:           return "xor";
    case BinOp::BitwiseOr:     return "bitwise_or";
    case BinOp::BitwiseAnd:    return "bitwise_and";
    case BinOp::BitwiseXor:    return "bitwise_xor";
    case BinOp::LeftShift:     return "left_shift";
    case BinOp::RightShift:    return "right_shift";
    case BinOp::OrTest:        return "or_test";
    case BinOp::AndTest:       return "and_test";
    case BinOp::Assign:        return "assign";
    case BinOp::Equals:        return "==";
    case BinOp::NotEquals:     return "!=";
    case BinOp::LessThan:      return "<";
    case BinOp::GreaterThan:   return ">";
    case BinOp::LessEquals:    return "<=";
    case BinOp::GreaterEquals: return ">=";
    case BinOp::In:            return "in";
    case BinOp::NotIn:         return "not_in";
    case BinOp::Is:            return "is";
    case BinOp::IsNot:         return "is_not";
    case BinOp::DictLookup:    return "dict_lookup";
    case BinOp::TmpAssignArg:  return "tmp_assign_arg";
    }

    return "???";
}

std::any AstExprBinary::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprDict::~AstExprDict()
{
}

std::any AstExprDict::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprDictPair::~AstExprDictPair()
{
}

std::any AstExprDictPair::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprSet::~AstExprSet()
{
}

std::any AstExprSet::accept(AstVisitor &v) 
{
    return v.visit(*this); 
}

AstExprUnary::~AstExprUnary()
{
}

const char *AstExprUnary::getNodeType() const
{
    switch (type_)
    {
    case Op::NotTest:    return "not_test";
    case Op::Star:       return "star";
    case Op::StarStar:   return "starstar";
    case Op::Positive:   return "positive";
    case Op::Negative:   return "negative";
    case Op::BitwiseNot: return "bitwise_not";
    case Op::Await:      return "await";
    }

    return "???";
}

std::any AstExprUnary::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprConditional::~AstExprConditional()
{
}

std::any AstExprConditional::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprVariableAnnotation::~AstExprVariableAnnotation()
{
}

std::any AstExprVariableAnnotation::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprTestlist::~AstExprTestlist()
{
}

std::any AstExprTestlist::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprFuncCall::~AstExprFuncCall()
{
}

std::any AstExprFuncCall::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprSlice::~AstExprSlice()
{
}

std::any AstExprSlice::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprList::~AstExprList()
{
}

std::any AstExprList::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprValue::~AstExprValue()
{
}

std::any AstExprValue::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprBoolean::~AstExprBoolean()
{
}

std::any AstExprBoolean::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprNumber::~AstExprNumber()
{
}

std::any AstExprNumber::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprString::~AstExprString()
{
}

std::any AstExprString::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprNone::~AstExprNone()
{
}

std::any AstExprNone::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprSymbol::~AstExprSymbol()
{
}

std::any AstExprSymbol::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprEllipsis::~AstExprEllipsis()
{
}

std::any AstExprEllipsis::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstTmpCompFor::~AstTmpCompFor()
{
}

AstExprCompFor::~AstExprCompFor()
{
}

std::any AstExprCompFor::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstTmpCompIf::~AstTmpCompIf()
{
}

AstExprTmpMemberLookup::~AstExprTmpMemberLookup()
{
}

AstExprMemberLookup::~AstExprMemberLookup()
{
}

std::any AstExprMemberLookup::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

AstExprTmpUnary::~AstExprTmpUnary()
{
}

const char *AstExprTmpUnary::getNodeType() const 
{
    switch (type_)
    {
    case Op::TmpStarArg:     return "tmp_star_arg";
    case Op::TmpStarStarArg: return "tmp_star_star_arg";
    case Op::TmpFuncCall:    return "tmp_func_call";
    case Op::TmpDictLookup:  return "tmp_dict_lookup";
    }

    return "???";
}

AstDecorator::~AstDecorator()
{
}

std::any AstDecorator::accept(AstVisitor &v) 
{ 
    return v.visit(*this); 
}

} // namespace fastpython.
