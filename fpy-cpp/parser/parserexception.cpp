#include "parserexception.h"


namespace fastpython
{


ParserException::ParserException(const std::string &message) 
    : line_(0), column_(0), message_(message) 
{
}


ParserException::ParserException(size_t line, size_t column, const std::string &message)
    : line_(line), column_(column), message_(message)
{
}


ParserException::~ParserException()
{
}


}
