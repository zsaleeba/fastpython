#ifndef ASTVISITOR_H
#define ASTVISITOR_H

#include <any>

#include "ast.h"


namespace fastpython {

    class AstVisitor
    {
    public:
    //    explicit AstVisitor() {}
        virtual ~AstVisitor() {}
        
        // Visitor pattern methods.        
        virtual std::any visit(AstStmt &ast) = 0;
        virtual std::any visit(AstStmtBlock &ast) = 0;
        virtual std::any visit(AstStmtFuncDef &ast) = 0;
        virtual std::any visit(AstStmtIf &ast) = 0;
        virtual std::any visit(AstStmtWhile &ast) = 0;
        virtual std::any visit(AstStmtFor &ast) = 0;
        virtual std::any visit(AstStmtDel &ast) = 0;
        virtual std::any visit(AstStmtPass &ast) = 0;
        virtual std::any visit(AstStmtBreak &ast) = 0;
        virtual std::any visit(AstStmtContinue &ast) = 0;
        virtual std::any visit(AstStmtReturn &ast) = 0;
        virtual std::any visit(AstStmtYield &ast) = 0;
        virtual std::any visit(AstStmtRaise &ast) = 0;
        virtual std::any visit(AstStmtTry &ast) = 0;
        virtual std::any visit(AstStmtWith &ast) = 0;
        virtual std::any visit(AstStmtLambdef &ast) = 0;
        virtual std::any visit(AstExpr &ast) = 0;
        virtual std::any visit(AstExprBinary &ast) = 0;
        virtual std::any visit(AstExprFuncCall &ast) = 0;
        virtual std::any visit(AstExprConditional &ast) = 0;
        virtual std::any visit(AstExprVariableAnnotation &ast) = 0;
        virtual std::any visit(AstExprSlice &ast) = 0;
        virtual std::any visit(AstExprList &ast) = 0;
        virtual std::any visit(AstExprTestlist &ast) = 0;
        virtual std::any visit(AstStmtClassDef &ast) = 0;
        virtual std::any visit(AstStmtImport &ast) = 0;
        virtual std::any visit(AstStmtImportFrom &ast) = 0;
        virtual std::any visit(AstStmtGlobal &ast) = 0;
        virtual std::any visit(AstStmtNonLocal &ast) = 0;
        virtual std::any visit(AstStmtAssert &ast) = 0;
        virtual std::any visit(AstArgList &ast) = 0;
        virtual std::any visit(AstExprSymbol &ast) = 0;
        virtual std::any visit(AstExprValue &ast) = 0;
        virtual std::any visit(AstExprNone &ast) = 0;
        virtual std::any visit(AstExprBoolean &ast) = 0;
        virtual std::any visit(AstExprNumber &ast) = 0;
        virtual std::any visit(AstExprString &ast) = 0;
        virtual std::any visit(AstExprEllipsis &ast) = 0;
        virtual std::any visit(AstExprCompFor &ast) = 0;
        virtual std::any visit(AstExprCompIf &ast) = 0;
        virtual std::any visit(AstExprMemberLookup &ast) = 0;
        virtual std::any visit(AstExprDict &ast) = 0;
        virtual std::any visit(AstExprDictPair &ast) = 0;
        virtual std::any visit(AstExprSet &ast) = 0;
        virtual std::any visit(AstDecorator &ast) = 0;
    };

}

#endif // ASTVISITOR_H
