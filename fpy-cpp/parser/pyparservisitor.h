#ifndef PYPARSERVISITOR_H
#define PYPARSERVISITOR_H


#include "antlr4-runtime.h"
#include "Python3Visitor.h"


namespace fastpython {
    
    /**
     * This class defines a concrete visitor for a parse tree
     * produced by Python3Parser.
     */
    class PyParserVisitor : public Python3Visitor {
    public:
    
      /**
       * Visit parse trees produced by Python3Parser.
       */
        virtual antlrcpp::Any visitSingle_input(Python3Parser::Single_inputContext *context) override;
    
        virtual antlrcpp::Any visitFile_input(Python3Parser::File_inputContext *context) override;
    
        virtual antlrcpp::Any visitEval_input(Python3Parser::Eval_inputContext *context) override;
    
        virtual antlrcpp::Any visitDecorator(Python3Parser::DecoratorContext *context) override;
    
        virtual antlrcpp::Any visitDecorators(Python3Parser::DecoratorsContext *context) override;
    
        virtual antlrcpp::Any visitDecorated(Python3Parser::DecoratedContext *context) override;
    
        virtual antlrcpp::Any visitAsync_funcdef(Python3Parser::Async_funcdefContext *context) override;
    
        virtual antlrcpp::Any visitFuncdef(Python3Parser::FuncdefContext *context) override;
    
        virtual antlrcpp::Any visitParameters(Python3Parser::ParametersContext *context) override;
    
        virtual antlrcpp::Any visitTypedargslist(Python3Parser::TypedargslistContext *context) override;
    
        virtual antlrcpp::Any visitTfpassign(Python3Parser::TfpassignContext *context) override;
    
        virtual antlrcpp::Any visitTfpdef(Python3Parser::TfpdefContext *context) override;
    
        virtual antlrcpp::Any visitVarargslist(Python3Parser::VarargslistContext *context) override;
    
        virtual antlrcpp::Any visitVfpass(Python3Parser::VfpassContext *context) override;
    
        virtual antlrcpp::Any visitVfpdef(Python3Parser::VfpdefContext *context) override;
    
        virtual antlrcpp::Any visitStmt(Python3Parser::StmtContext *context) override;
    
        virtual antlrcpp::Any visitSimple_stmt(Python3Parser::Simple_stmtContext *context) override;
    
        virtual antlrcpp::Any visitSmall_stmt(Python3Parser::Small_stmtContext *context) override;
    
        virtual antlrcpp::Any visitExpr_stmt(Python3Parser::Expr_stmtContext *context) override;
    
        virtual antlrcpp::Any visitConv_expr_stmt_clause(Python3Parser::Conv_expr_stmt_clauseContext *context) override;
    
        virtual antlrcpp::Any visitAnnassign(Python3Parser::AnnassignContext *context) override;
    
        virtual antlrcpp::Any visitTestlist_star_expr(Python3Parser::Testlist_star_exprContext *context) override;
    
        virtual antlrcpp::Any visitTestlist_star_clause(Python3Parser::Testlist_star_clauseContext *context) override;
    
        virtual antlrcpp::Any visitAugassign(Python3Parser::AugassignContext *context) override;
    
        virtual antlrcpp::Any visitDel_stmt(Python3Parser::Del_stmtContext *context) override;
    
        virtual antlrcpp::Any visitPass_stmt(Python3Parser::Pass_stmtContext *context) override;
    
        virtual antlrcpp::Any visitFlow_stmt(Python3Parser::Flow_stmtContext *context) override;
    
        virtual antlrcpp::Any visitBreak_stmt(Python3Parser::Break_stmtContext *context) override;
    
        virtual antlrcpp::Any visitContinue_stmt(Python3Parser::Continue_stmtContext *context) override;
    
        virtual antlrcpp::Any visitReturn_stmt(Python3Parser::Return_stmtContext *context) override;
    
        virtual antlrcpp::Any visitYield_stmt(Python3Parser::Yield_stmtContext *context) override;
    
        virtual antlrcpp::Any visitRaise_stmt(Python3Parser::Raise_stmtContext *context) override;
    
        virtual antlrcpp::Any visitImport_stmt(Python3Parser::Import_stmtContext *context) override;
    
        virtual antlrcpp::Any visitImport_name(Python3Parser::Import_nameContext *context) override;
    
        virtual antlrcpp::Any visitImport_from(Python3Parser::Import_fromContext *context) override;
    
        virtual antlrcpp::Any visitImport_dots(Python3Parser::Import_dotsContext *context) override;
    
        virtual antlrcpp::Any visitImport_from_names(Python3Parser::Import_from_namesContext *context) override;

        virtual antlrcpp::Any visitImport_as_name(Python3Parser::Import_as_nameContext *context) override;
    
        virtual antlrcpp::Any visitDotted_as_name(Python3Parser::Dotted_as_nameContext *context) override;
    
        virtual antlrcpp::Any visitImport_as_names(Python3Parser::Import_as_namesContext *context) override;
    
        virtual antlrcpp::Any visitDotted_as_names(Python3Parser::Dotted_as_namesContext *context) override;
    
        virtual antlrcpp::Any visitDotted_name(Python3Parser::Dotted_nameContext *context) override;
    
        virtual antlrcpp::Any visitGlobal_stmt(Python3Parser::Global_stmtContext *context) override;
    
        virtual antlrcpp::Any visitNonlocal_stmt(Python3Parser::Nonlocal_stmtContext *context) override;
    
        virtual antlrcpp::Any visitAssert_stmt(Python3Parser::Assert_stmtContext *context) override;
    
        virtual antlrcpp::Any visitCompound_stmt(Python3Parser::Compound_stmtContext *context) override;
    
        virtual antlrcpp::Any visitAsync_stmt(Python3Parser::Async_stmtContext *context) override;
    
        virtual antlrcpp::Any visitIf_stmt(Python3Parser::If_stmtContext *context) override;
    
        virtual antlrcpp::Any visitWhile_stmt(Python3Parser::While_stmtContext *context) override;
    
        virtual antlrcpp::Any visitFor_stmt(Python3Parser::For_stmtContext *context) override;
    
        virtual antlrcpp::Any visitTry_stmt(Python3Parser::Try_stmtContext *context) override;
    
        virtual antlrcpp::Any visitWith_stmt(Python3Parser::With_stmtContext *context) override;
    
        virtual antlrcpp::Any visitWith_item(Python3Parser::With_itemContext *context) override;
    
        virtual antlrcpp::Any visitExcept_clause(Python3Parser::Except_clauseContext *context) override;
    
        virtual antlrcpp::Any visitSuite(Python3Parser::SuiteContext *context) override;
    
        virtual antlrcpp::Any visitTest(Python3Parser::TestContext *context) override;
    
        virtual antlrcpp::Any visitTest_nocond(Python3Parser::Test_nocondContext *context) override;
    
        virtual antlrcpp::Any visitLambdef(Python3Parser::LambdefContext *context) override;
    
        virtual antlrcpp::Any visitLambdef_nocond(Python3Parser::Lambdef_nocondContext *context) override;
    
        virtual antlrcpp::Any visitOr_test(Python3Parser::Or_testContext *context) override;
    
        virtual antlrcpp::Any visitAnd_test(Python3Parser::And_testContext *context) override;
    
        virtual antlrcpp::Any visitNot_test(Python3Parser::Not_testContext *context) override;
    
        virtual antlrcpp::Any visitComparison(Python3Parser::ComparisonContext *context) override;
    
        virtual antlrcpp::Any visitComp_op(Python3Parser::Comp_opContext *context) override;
    
        virtual antlrcpp::Any visitStar_expr(Python3Parser::Star_exprContext *context) override;
        
        virtual antlrcpp::Any visitStar_star_expr(Python3Parser::Star_star_exprContext *context) override;
    
        virtual antlrcpp::Any visitExpr(Python3Parser::ExprContext *context) override;
    
        virtual antlrcpp::Any visitXor_expr(Python3Parser::Xor_exprContext *context) override;
    
        virtual antlrcpp::Any visitAnd_expr(Python3Parser::And_exprContext *context) override;
    
        virtual antlrcpp::Any visitShift_expr(Python3Parser::Shift_exprContext *context) override;
    
        virtual antlrcpp::Any visitArith_expr(Python3Parser::Arith_exprContext *context) override;
    
        virtual antlrcpp::Any visitTerm(Python3Parser::TermContext *context) override;
    
        virtual antlrcpp::Any visitFactor(Python3Parser::FactorContext *context) override;
    
        virtual antlrcpp::Any visitPower(Python3Parser::PowerContext *context) override;
    
        virtual antlrcpp::Any visitAtom_expr(Python3Parser::Atom_exprContext *context) override;
    
        virtual antlrcpp::Any visitAtom(Python3Parser::AtomContext *context) override;
    
        virtual antlrcpp::Any visitTestlist_comp(Python3Parser::Testlist_compContext *context) override;
    
        virtual antlrcpp::Any visitTrailer(Python3Parser::TrailerContext *context) override;
    
        virtual antlrcpp::Any visitSubscriptlist(Python3Parser::SubscriptlistContext *context) override;
    
        virtual antlrcpp::Any visitSubscript(Python3Parser::SubscriptContext *context) override;
    
        virtual antlrcpp::Any visitSliceop(Python3Parser::SliceopContext *context) override;
    
        virtual antlrcpp::Any visitExprlist(Python3Parser::ExprlistContext *context) override;
    
        virtual antlrcpp::Any visitExprlist_item(Python3Parser::Exprlist_itemContext *context) override;

        virtual antlrcpp::Any visitTestlist_elem(Python3Parser::Testlist_elemContext *context) override;
        
        virtual antlrcpp::Any visitTestlist(Python3Parser::TestlistContext *context) override;
    
        virtual antlrcpp::Any visitDictorsetmaker(Python3Parser::DictorsetmakerContext *context) override;
    
        virtual antlrcpp::Any visitDictmaker(Python3Parser::DictmakerContext *context) override;
    
        virtual antlrcpp::Any visitDictterm(Python3Parser::DicttermContext *context) override;
    
        virtual antlrcpp::Any visitDictssterm(Python3Parser::DictsstermContext *context) override;
    
        virtual antlrcpp::Any visitSetmaker(Python3Parser::SetmakerContext *context) override;
    
        virtual antlrcpp::Any visitSetterm(Python3Parser::SettermContext *context) override;
    
        virtual antlrcpp::Any visitClassdef(Python3Parser::ClassdefContext *context) override;
    
        virtual antlrcpp::Any visitArglist(Python3Parser::ArglistContext *context) override;
    
        virtual antlrcpp::Any visitArgument(Python3Parser::ArgumentContext *context) override;
    
        virtual antlrcpp::Any visitComp_iter(Python3Parser::Comp_iterContext *context) override;
    
        virtual antlrcpp::Any visitComp_for(Python3Parser::Comp_forContext *context) override;
    
        virtual antlrcpp::Any visitComp_if(Python3Parser::Comp_ifContext *context) override;
    
        virtual antlrcpp::Any visitEncoding_decl(Python3Parser::Encoding_declContext *context) override;
    
        virtual antlrcpp::Any visitYield_expr(Python3Parser::Yield_exprContext *context) override;
    };

}  // namespace fastpython


#endif // PYPARSERVISITOR_H
