#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <memory>


namespace fastpython
{

    // Forward declarations.
    class Ast;
    class IR;
    

    //
    // This class performs semantic analysis on the parsed AST.
    // It produces an Intermediate Representation (IR) of the code as a
    // result.
    //

    class Analysis
    {
    private:
        // Abstract Syntax Tree from the parser.
        std::shared_ptr<Ast> ast_;
        
        // Intermediate representation.
        std::shared_ptr<IR> ir_;
        
    private:
        // Converts the AST to IR.
        std::shared_ptr<IR> convertToIr(std::shared_ptr<Ast> ast_);
        
    public:
        Analysis(std::shared_ptr<Ast> ast);
        ~Analysis();
        
        std::shared_ptr<IR> analyse();
    };

}

#endif // ANALYSIS_H
