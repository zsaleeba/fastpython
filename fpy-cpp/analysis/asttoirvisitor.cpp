#include "ast.h"
#include "asttoirvisitor.h"

namespace fastpython
{

AstToIrVisitor::AstToIrVisitor()
{
    
}


AstToIrVisitor::~AstToIrVisitor()
{
    
}

std::any AstToIrVisitor::visit(AstStmt &ast)
{
    std::cout << "stmt\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtBlock &ast)
{
    std::vector<std::any> ch;
    ch.reserve(ast.getChildren().size());
            
    for (auto &stmt: ast.getChildren())
    {
        ch.push_back(stmt->accept(*this));
    }
    
    std::cout << "stmt block\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtFuncDef &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtIf &ast)
{
    for (auto &expr: ast.getTests())
    {
        expr->accept(*this);
    }
    
    for (auto &stmt: ast.getSuites())
    {
        stmt->accept(*this);
    }
    
    if (ast.getElseSuite())
    {
        ast.getElseSuite()->accept(*this);
    }
    
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtWhile &ast)
{
    std::cout << "stmt while\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtFor &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtDel &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtPass &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtBreak &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtContinue &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtReturn &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtYield &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtRaise &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtTry &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtWith &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtLambdef &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExpr &ast)
{
    std::cout << "expr\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprBinary &ast)
{
    std::cout << "expr binop " << ast.getNodeType() << "\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprFuncCall &ast)
{
    std::cout << "expr call\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprConditional &ast)
{
    std::cout << "expr cond\n";
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprVariableAnnotation &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprSlice &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprList &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprTestlist &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtClassDef &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtImport &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtImportFrom &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtGlobal &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtNonLocal &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstStmtAssert &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstArgList &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprSymbol &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprValue &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprNone &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprBoolean &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprNumber &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprString &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprEllipsis &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprCompFor &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprCompIf &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprMemberLookup &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprDict &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprDictPair &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstExprSet &ast)
{
    return nullptr;
}

std::any AstToIrVisitor::visit(AstDecorator &ast)
{
    return nullptr;
}


} // namespace fastpython.
