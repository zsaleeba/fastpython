#ifndef ASTTOIRVISITOR_H
#define ASTTOIRVISITOR_H

#include <memory>
#include <any>
#include <vector>

#include "astvisitor.h"
#include "ir.h"

namespace fastpython
{

    class AstToIrVisitor : public AstVisitor
    {
    private:
        std::shared_ptr<IR> ir;
        
    public:
        AstToIrVisitor();
        virtual ~AstToIrVisitor() override;
        
        // Visitor pattern methods.
        virtual std::any visit(AstStmt &ast) override;
        virtual std::any visit(AstStmtBlock &ast) override;
        virtual std::any visit(AstStmtFuncDef &ast) override;
        virtual std::any visit(AstStmtIf &ast) override;
        virtual std::any visit(AstStmtWhile &ast) override;
        virtual std::any visit(AstStmtFor &ast) override;
        virtual std::any visit(AstStmtDel &ast) override;
        virtual std::any visit(AstStmtPass &ast) override;
        virtual std::any visit(AstStmtBreak &ast) override;
        virtual std::any visit(AstStmtContinue &ast) override;
        virtual std::any visit(AstStmtReturn &ast) override;
        virtual std::any visit(AstStmtYield &ast) override;
        virtual std::any visit(AstStmtRaise &ast) override;
        virtual std::any visit(AstStmtTry &ast) override;
        virtual std::any visit(AstStmtWith &ast) override;
        virtual std::any visit(AstStmtLambdef &ast) override;
        virtual std::any visit(AstExpr &ast) override;
        virtual std::any visit(AstExprBinary &ast) override;
        virtual std::any visit(AstExprFuncCall &ast) override;
        virtual std::any visit(AstExprConditional &ast) override;
        virtual std::any visit(AstExprVariableAnnotation &ast) override;
        virtual std::any visit(AstExprSlice &ast) override;
        virtual std::any visit(AstExprList &ast) override;
        virtual std::any visit(AstExprTestlist &ast) override;
        virtual std::any visit(AstStmtClassDef &ast) override;
        virtual std::any visit(AstStmtImport &ast) override;
        virtual std::any visit(AstStmtImportFrom &ast) override;
        virtual std::any visit(AstStmtGlobal &ast) override;
        virtual std::any visit(AstStmtNonLocal &ast) override;
        virtual std::any visit(AstStmtAssert &ast) override;
        virtual std::any visit(AstArgList &ast) override;
        virtual std::any visit(AstExprSymbol &ast) override;
        virtual std::any visit(AstExprValue &ast) override;
        virtual std::any visit(AstExprNone &ast) override;
        virtual std::any visit(AstExprBoolean &ast) override;
        virtual std::any visit(AstExprNumber &ast) override;
        virtual std::any visit(AstExprString &ast) override;
        virtual std::any visit(AstExprEllipsis &ast) override;
        virtual std::any visit(AstExprCompFor &ast) override;
        virtual std::any visit(AstExprCompIf &ast) override;
        virtual std::any visit(AstExprMemberLookup &ast) override;
        virtual std::any visit(AstExprDict &ast) override;
        virtual std::any visit(AstExprDictPair &ast) override;
        virtual std::any visit(AstExprSet &ast) override;
        virtual std::any visit(AstDecorator &ast) override;
        
        // Accessors.
        std::shared_ptr<IR> getIr() { return ir; }
    };

} // namespace fastpython.

#endif // ASTTOIRVISITOR_H
