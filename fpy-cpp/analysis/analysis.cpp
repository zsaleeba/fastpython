#include <memory>

#include "analysis.h"
#include "ast.h"
#include "ir.h"
#include "asttoirvisitor.h"


namespace fastpython
{


//
// This class performs semantic analysis on the parsed AST.
// It produces an Internal Representation (IR) of the code as a
// result.
//

Analysis::Analysis(std::shared_ptr<Ast> ast)
    : ast_(ast)
{
}


Analysis::~Analysis()
{    
}


std::shared_ptr<IR> Analysis::analyse()
{
    ir_ = convertToIr(ast_);
    
    return ir_;
}


//
// Convert the AST from the parser to IR form.
//
// Uses the technique described in:
//  "Simple and Efficient Construction of Static Single Assignment Form"
//  by Matthias Braun, Sebastian Buchwald, Sebastian Hack, Roland Leißa,
//  Christoph Mallon, and Andreas Zwinkau
//

std::shared_ptr<IR> Analysis::convertToIr(std::shared_ptr<Ast> ast)
{
    AstToIrVisitor v;
    
    ast->accept(v);
    
    return v.getIr();
}


}
