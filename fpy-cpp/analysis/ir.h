//
// The Ir class represents an Intermediate Representation of the code.
// The AST is converted into IR, which is the internal form of the code
// used for most of the semantic analysis and optimisations. The IR is  
// later used as input for LLVM.
//
// This IR is designed to match well with Python semantics. It's a DAG
// (Directed Acyclic Graph) representation of the code with SSA
// (Static Single Assignment) form.
//
// Useful links on the IR:
// 
// https://en.wikipedia.org/wiki/Intermediate_representation
// https://en.wikipedia.org/wiki/Static_single_assignment_form
// https://en.wikipedia.org/wiki/Directed_acyclic_graph
//

#ifndef IR_H
#define IR_H

#include <memory>
#include <vector>

#include "op.h"


namespace fastpython {

    // Forward declarations.
    class Value;
    class IrBinOp;
    class IrStmt;
    class IrBlock;
    class IrControlFlow;
    class IrBasicBlock;


    //
    // The intermediate representation of a program.
    // 
    // This is a series of python statements.
    //
    
    class IR
    {
        std::vector< std::shared_ptr<IrStmt> > tlDecls_;
        
    public:
        IR();
        ~IR();
        
        // Add a top level decl to the IR.
        void add(std::shared_ptr<IrStmt> &decl);
    };
    
    
    //
    // A top level python declaration.
    // 
    // This may be a class definition or any other statement.
    //
    
    class IrStmt
    {
    public:
        IrStmt();
        virtual ~IrStmt();
    };
    
    
    //
    // A class def defines a class.
    //
    
    class IrClassDef : public IrStmt
    {
    private:
        std::string name_;
        std::shared_ptr<IrBlock> body_;
        
    public:
        IrClassDef();
        virtual ~IrClassDef();
    };
    
    
#if 0
    //
    // A block of code. May contain control flow statements and
    // nested blocks.
    //
    
    class IrBlock : public IrStmt
    {
    private:
        std::shared_ptr<IrBasicBlock> basicBlock_;
        
    public:
        IrBlock();
        virtual ~IrBlock();
    };
#endif
    
    
    //
    // A basic block is a series of instructions none of which perform
    // flow control.
    //
    
    class IrBasicBlock : public IrStmt
    {
    private:
        // The instructions in this basic block.
        std::vector<IrStmt> instrs_;
        
        // Control flow after this basic block.
        std::shared_ptr<IrControlFlow> controlFlow_;
        
    public:
        IrBasicBlock();
        virtual ~IrBasicBlock();
    };
    
    
    //
    // At the end of each basic block is a control flow statement which determines
    // what happens next. This may be a direct jump to another basic block or it 
    // may be an 'if' or a loop control flow.
    //
    
    class IrControlFlow
    {
    public:
        IrControlFlow();
        virtual ~IrControlFlow();
    };
    
    
    //
    // Control flow directly to another basic block. Must always be forward
    // as this is a DAG.
    //
    
    class IrControlFlowDirect : public IrControlFlow
    {
        std::shared_ptr<IrBasicBlock> to_;
        
    public:
        IrControlFlowDirect();
        virtual ~IrControlFlowDirect();
    };
    
    
    //
    // Control flow of an 'if' statement.
    //
    
    class IrControlFlowIf : public IrControlFlow
    {
        std::weak_ptr<Value> condition_;
        std::shared_ptr<IrBasicBlock> then_;
        std::shared_ptr<IrBasicBlock> else_;
        
    public:
        IrControlFlowIf();
        virtual ~IrControlFlowIf();
    };
    
    
    //
    // Control flow for conditionally branching backwards. Commonly used
    // for looping. Don't use this to branch forwards since it won't
    // manage memory ownership correctly. Use IrControlFlowDirect instead
    // in that case.
    //
    
    class IrControlFlowConditionalBranchBackwards : public IrControlFlow
    {
        std::weak_ptr<Value> condition_;
        std::weak_ptr<IrBasicBlock> loopStart_;
        
    public:
        IrControlFlowConditionalBranchBackwards();
        virtual ~IrControlFlowConditionalBranchBackwards();
    };
    
    
    //
    // A single instruction within the IR.
    //
    
    class IrBinOp : public IrStmt
    {
    private:
        // The opcode.
        BinOp op_;
        
        // Arguments.
        std::shared_ptr<Value> left_;
        std::shared_ptr<Value> right_;
        
        // Result.
        std::shared_ptr<Value> result_;
        
    public:
        IrBinOp(BinOp op);
        virtual ~IrBinOp();
        
    };
    
    
    //
    // A single instruction within the IR.
    //
    
    class IrUnOp : public IrStmt
    {
    private:
        // The opcode.
        UnOp op_;
        
        // Arguments.
        std::shared_ptr<Value> arg_;
        
        // Result.
        std::shared_ptr<Value> result_;
        
    public:
        IrUnOp(UnOp op);
        virtual ~IrUnOp();
        
    };

}

#endif // IR_H
