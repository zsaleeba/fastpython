#ifndef OPTIMISER_H
#define OPTIMISER_H

#include "ir.h"


namespace fastpython 
{

    // Forward declarations.
    class IR;


    //
    // The optimiser takes the IR and performs a series of optimisation
    // phases on it.
    //
    
    class Optimiser
    {
        std::shared_ptr<IR> ir_;
        
    public:
        Optimiser(std::shared_ptr<IR> ir);
        ~Optimiser();
        
        std::shared_ptr<IR> optimise();
    };
    

}


#endif // OPTIMISER_H
