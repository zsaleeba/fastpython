#include "ir.h"
#include "optimiser.h"


namespace fastpython 
{

//
// The optimiser takes the IR and performs a series of optimisation
// phases on it.
//

Optimiser::Optimiser(std::shared_ptr<IR> ir)
    : ir_(ir)
{
}


Optimiser::~Optimiser()
{
}


std::shared_ptr<IR> Optimiser::optimise()
{
    return nullptr;
}
    

}
