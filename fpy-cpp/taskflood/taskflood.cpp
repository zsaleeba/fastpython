#include <thread>

#include "threadpool.h"
#include "taskflood.h"


namespace taskflood 
{


TaskFlood::TaskFlood() 
    : pool_(std::thread::hardware_concurrency())
{
}


//
// Sends a message to a taskable instance.
//
void TaskFlood::sendMessage(TaskObjPtr obj, std::function<void()> msg)
{
    pool_.enqueueTask(obj, msg);
}


} // namespace taskflood
