#include <algorithm>

#include "threadpool.h"


namespace taskflood 
{

//
// Constructor.
//

ThreadPool::ThreadPool(unsigned numThreads)
    : numThreads_(numThreads),
      numThreadsReady_(numThreads),
      threads_(numThreads)
{
}


//
// Adds a ready-to-run task to the queue of tasks to be performed.
//

void ThreadPool::enqueueTask(TaskObjPtr obj, std::function<void()> func)
{
    std::unique_lock<std::mutex> lock(mutex_);
    
    readyQueue_.emplace_back( QueueEntry(obj, func) );

    // Tell a thread to grab it and run.
    startTask_.notify_one();
}


//
// Each thread pool thread runs this to repeatedly fetch tasks and
// run them.
//

void ThreadPool::threadMain()
{
    while (true)
    {
        // Wait for a task to be sent to us.
        std::unique_lock<std::mutex> lock(mutex_);
        startTask_.wait(lock, [&]{ return !readyQueue_.empty(); });
        
        // Eliminate any inactive tasks from the head of the queue.
        while (!readyQueue_.empty() && !readyQueue_.front().getActive())
        {
            readyQueue_.pop_front();
        }

        if (readyQueue_.empty())        
            continue;   // There's nothing to do.
        
        // Scan the queue for a task we can execute.
        auto pos = readyQueue_.begin();
        while (pos != readyQueue_.end() && (busyObjs_.find(pos->getObj()) != busyObjs_.end() || !pos->getActive()))
        {
            pos++;
        }

        // If we found one run it.
        if (pos == readyQueue_.end() || busyObjs_.find(pos->getObj()) != busyObjs_.end() || !pos->getActive())
            continue;   // Didn't find one, there's nothing to do.

        // Get the task details.
        auto obj  = pos->getObj();
        auto func = pos->getFunc();
        
        // Remove the task depending on where it is in the queue.
        // Doesn't actually remove tasks in the middle of the queue, 
        // just mark them inactive so they can be removed more
        // efficiently later.
        if (pos == readyQueue_.begin())
        {
            readyQueue_.pop_front();
        }
        else if (next(pos) == readyQueue_.end())
        {
            readyQueue_.pop_back();
        }
        else
        {
            pos->setActive(false);
        }
        
        // Note that this object is now busy.
        busyObjs_.insert(obj);
        numThreadsReady_--;
        
        // Unlock the mutex so other threads can access the queue.
        lock.unlock();
        
        // Execute the task.
        std::invoke(func);
        
        // Mark the object as not busy.
        lock.lock();
        busyObjs_.erase(obj);
        numThreadsReady_++;
        
        // Now we're done we know there's a thread available so try 
        // running another Task.
        if (!readyQueue_.empty())
        {
            lock.unlock();
            startTask_.notify_one();
        }
    }
}


} // namespace taskflood.
