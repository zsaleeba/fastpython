#ifndef TASKFLOOD_NOTIFIER_H
#define TASKFLOOD_NOTIFIER_H

#include "task.h"


namespace taskflood 
{
    //
    // Notifier is used to wake up a task when a set of dependencies
    // is satisfied. This is useful when a task needs a certain set
    // of preconditions to be satisfied before it can continue.
    //
    // Notifier listens to messages reflecting system state and marks 
    // off dependencies as they're satisfied. When all of them are 
    // satisfied messages are sent to whichever tasks are watching.
    //
    // An example of using Notifier is if you're writing a compiler
    // which has compiled a file to a certain point but is waiting on
    // some global declarations to be resolved from other modules
    // before it can continue. It'd set up a notifier to wait on
    // messages announcing that the dependent global variables have
    // been resolved. Then it'll send a message to the compiler saying
    // that it can continue.
    //

    class Notifier : public Task
    {
    public:
        Notifier();
    };

} // namespace taskflood

#endif // TASKFLOOD_NOTIFIER_H
