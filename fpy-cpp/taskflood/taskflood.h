////////////////////////////////////////////////////////////////////////////
///                                                                      ///
///      Velocity job-based, precondition-based parallelism system       ///
///                                                                      ///
////////////////////////////////////////////////////////////////////////////

//
// Velocity is a M:N job-based scheduler. It takes a set of short-running 
// jobs and schedules each of them to run concurrently on the available CPU 
// threads. 
//
//   * Programs are split into a large number of smaller "jobs", each of
//     which has preconditions which trigger its execution. Each job may
//     output indicators which serve as preconditions for other jobs.
//
//   * Jobs which aren't running don't use any stack space so the system 
//     uses much less memory than even "green" threads. Thousands or
//     millions of jobs can be scheduled without much impact on memory
//     usage.
//
//   * Jobs run concurrently.
//
//   * Jobs run to completion.
//
//   * The system is ideal for computational tasks which have inherent
//     parallelism.
//
//   * If low latency is desired the duration of the jobs should be kept
//     short.
//
//   * Programs become a dependency graph of jobs which link to other jobs
//     via their indicators and preconditions.
//
//   * To get good parallelism a decent number of jobs should be created
//     when the programs starts, many of which can potentially execute
//     simultaneously.
//

#ifndef TASKFLOOD_TASKFLOOD_H
#define TASKFLOOD_TASKFLOOD_H


#include <memory>
#include <set>
#include <functional>

#include "threadpool.h"


namespace taskflood 
{
    // Forward declarations.
    class Message;
    

    //
    // TaskFlood is the controller for the taskflood system.
    // It keeps track of all the Taskable objects, the queued 
    // messages and it schedules messages for execution as
    // required.
    //
    
    class TaskFlood
    {
    private:
        // The pool of hardware threads.
        ThreadPool pool_;
        
        // All the different types of task we can handle and all the objects that can run tasks.
        std::set<TaskObjPtr> taskObjects_;

        // Mutex for access to the above.
        std::mutex     taskMutex_;
                
    public:
        TaskFlood();
        
        //
        // Instances of tasks are added here.
        //
        // Parallelism is per-object. ie. Each instance will have only 
        // one message delivered to it at a time. Only when a previous
        // message handler has completed will a new message be delivered.
        //
        // For example say you have an image and you want to perform
        // a parallel blur() operation on it. To do this you make a 
        // sub-image class as a subclass of Taskable. Then you instantiate
        // this for each for each part of the image. When you want to
        // do the blur you send messages to all of the instances and
        // they all execute as parallel as the CPU can manage.
        //
        
        //
        // Sends a message to a task instance.
        //
        void sendMessage(TaskObjPtr obj, std::function<void()> msg);
    };
    
} // namespace taskflood

#endif // TASKFLOOD_TASKFLOOD_H
