#ifndef TASKFLOOD_THREADPOOL_H
#define TASKFLOOD_THREADPOOL_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <deque>
#include <set>
#include <functional>


namespace taskflood 
{
    // Forward declarations.
    typedef void *TaskObjPtr;

    
    //
    // An entry in the ready queue.
    //
    
    class QueueEntry
    {
    private:
        // The object this task is going to be executed on.
        TaskObjPtr            obj_;
        
        // The function with the code for this task.
        std::function<void()> func_;
        
        // Whether this queue entry is still active. Inactive entries have
        // already been executed and can be discarded.
        bool                  active_;
        
    public:
        // Constructor.
        explicit QueueEntry(TaskObjPtr obj, std::function<void()> func)
            : obj_(obj),
              func_(func),
              active_(true)
        {}
        
        // Accessors.
        TaskObjPtr               getObj() const    { return obj_; }
        std::function<void()> getFunc() const   { return func_; }
        bool                  getActive() const { return active_; }
        void                  setActive(bool active) { active_ = active; }
    };
    
    
    //
    // Manages a pool of OS threads which we multiplex Tasks on to.
    //
    
    class ThreadPool
    {
    private:
        // The number of threads we're capable of running at a time.
        unsigned                 numThreads_;
        
        // How many of the above threads are ready to run.
        unsigned                 numThreadsReady_;
        
        // All the threads we're running.
        std::vector<std::thread> threads_;
        
        // A queue of the tasks which are ready to be performed.
        std::deque<QueueEntry>   readyQueue_;
        
        // Busy objects - ie. objects which have a task running on them.
        std::set<TaskObjPtr>     busyObjs_;

        // Triggers the next available thread to start running a task.
        std::condition_variable  startTask_;
        
        // A mutex to access members of this class.
        std::mutex               mutex_;
        
    public:
        ThreadPool(unsigned numThreads);
        
        //
        // Adds a ready-to-run task to the queue of tasks to be performed.
        //
        void enqueueTask(TaskObjPtr obj, std::function<void()> func);
        
    private:
        //
        // Each thread runs this to repeatedly fetch tasks and run them.
        //
        void threadMain();
        
    protected:
        //
        // Notifies the system that a thread has completed and is ready for
        // a new task.
        //
        void threadCompleted(int threadId);
    };
    
} // namespace taskflood.

#endif // TASKFLOOD_THREADPOOL_H
