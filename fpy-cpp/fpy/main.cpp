#include <iostream>

#include "parser.h"
#include "analysis.h"
#include "optimiser.h"
#include "executable.h"

using namespace fastpython;


int main(int argc, char **argv)
{
    std::cout << "fpy\n";
    
    for (int i = 1; i < argc; i++)
    {
        Parser parser(argv[i]);
        auto ast = parser.parse();
        
        Analysis analysis(ast);
        auto ir = analysis.analyse();
        
        Optimiser optimiser(ir);
        auto optIr = optimiser.optimise();
    }

//    Executable exe("program");
//    exe.build();

    return 0;
}
