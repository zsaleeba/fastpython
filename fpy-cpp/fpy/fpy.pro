TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++1z

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../common/release/ -lcommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../common/debug/ -lcommon
else:unix: LIBS += -L$$OUT_PWD/../common/ -lparser

INCLUDEPATH += $$PWD/../common 
DEPENDPATH += $$PWD/../common

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../parser/release/ -lparser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../parser/debug/ -lparser
else:unix: LIBS += -L$$OUT_PWD/../parser/ -lparser

INCLUDEPATH += $$PWD/../parser 
DEPENDPATH += $$PWD/../parser

INCLUDEPATH += \
    /usr/local/include/antlr4-runtime \
    $$PWD/../parser/gen

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../codegen/release/libcodegen.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../codegen/debug/libcodegen.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../codegen/release/codegen.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../codegen/debug/codegen.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../codegen/libcodegen.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../analysis/release/ -lanalysis
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../analysis/debug/ -lanalysis
else:unix: LIBS += -L$$OUT_PWD/../analysis/ -lanalysis

INCLUDEPATH += $$PWD/../analysis
DEPENDPATH += $$PWD/../analysis

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../analysis/release/libanalysis.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../analysis/debug/libanalysis.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../analysis/release/analysis.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../analysis/debug/analysis.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../analysis/libanalysis.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../optimiser/release/ -loptimiser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../optimiser/debug/ -loptimiser
else:unix: LIBS += -L$$OUT_PWD/../optimiser/ -loptimiser

INCLUDEPATH += $$PWD/../optimiser
DEPENDPATH += $$PWD/../optimiser

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../optimiser/release/liboptimiser.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../optimiser/debug/liboptimiser.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../optimiser/release/optimiser.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../optimiser/debug/optimiser.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../optimiser/liboptimiser.a

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../parser/release/libparser.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../parser/debug/libparser.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../parser/release/parser.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../parser/debug/parser.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../parser/libparser.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../codegen/release/ -lcodegen
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../codegen/debug/ -lcodegen
else:unix: LIBS += -L$$OUT_PWD/../codegen/ -lcodegen

INCLUDEPATH += $$PWD/../codegen
DEPENDPATH += $$PWD/../codegen

unix|win32: LIBS += -lantlr4-runtime

unix|win32: LIBS += /usr/lib/llvm-6.0/lib/libLLVM.so

unix|win32: LIBS += -llmdb

unix|win32: LIBS += -lpthread
