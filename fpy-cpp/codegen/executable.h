#ifndef FPYTHON_EXECUTABLE_H
#define FPYTHON_EXECUTABLE_H

#include <string>
#include <memory>
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"


namespace fastpython {


    class Executable
    {
        std::string filename_;
        llvm::LLVMContext context_;
        llvm::IRBuilder<> builder_;
    
    private:
        std::unique_ptr<llvm::Module> buildModule();
        void writeModuleToFile(llvm::Module *module);
        void createExecutable();
        
    public:
        Executable(const std::string &filename);
    
        void build();
    };

}

#endif // FPYTHON_EXECUTABLE_H

