#include <fstream>
#include <cstdlib>

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/Support/raw_os_ostream.h"
#include "executable.h"


using namespace llvm;

namespace fastpython {



Executable::Executable(const std::string &filename) : filename_(filename), builder_(context_)
{
}


std::unique_ptr<Module> Executable::buildModule()
{
    auto module = std::make_unique<Module>(filename_, context_);

    /* Create main function */
    auto funcType = FunctionType::get(builder_.getInt32Ty(), false);    
    auto mainFunc = Function::Create(funcType, Function::ExternalLinkage, "main", module.get());
    auto entry = BasicBlock::Create(context_, "entrypoint", mainFunc);
    builder_.SetInsertPoint(entry);

    /* String constant */
    Value *helloWorldStr = builder_.CreateGlobalStringPtr("hello world!\n");

    /* Create "puts" function */
    std::vector<Type *> putsArgs;
    putsArgs.push_back(builder_.getInt8Ty()->getPointerTo());
    ArrayRef<Type*> argsRef(putsArgs);
    auto putsType = FunctionType::get(builder_.getInt32Ty(), argsRef, false);
    auto putsFunc = module->getOrInsertFunction("puts", putsType);

    /* Invoke it */
    builder_.CreateCall(putsFunc, helloWorldStr);

    /* Return zero */
    builder_.CreateRet(ConstantInt::get(context_, APInt(32, 0)));

    return module;
}


void Executable::writeModuleToFile(Module *module)
{
    // XXX we should use TargetMachine::addPassesToEmitFile() here to output object code instead.
    std::ofstream std_file_stream(filename_ + ".ll");
    raw_os_ostream file_stream(std_file_stream);
    module->print(file_stream, nullptr);
}


void Executable::createExecutable()
{
    // Run llc to compile/assemble it.
    auto llcCommand = std::string("llc -filetype=obj ") + filename_ + ".ll";
    if (system(llcCommand.c_str()) != 0)
        return;

    // Run clang to link it.
    auto linkCommand = std::string("clang -o ") + filename_ + " " + filename_ + ".o";
    system(linkCommand.c_str());
}


void Executable::build()
{
    std::unique_ptr<Module> module = buildModule();
    writeModuleToFile(module.get());
    createExecutable();
}

}
