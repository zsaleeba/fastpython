#ifndef OP_H
#define OP_H

namespace fastpython {

    enum class BinOp
    {
        Add, Sub, Mult, At, Div, Mod, Power, IDiv,
        Or, Xor, And,
        LeftShift, RightShift, 
        OrTest, AndTest,
        BitwiseOr, BitwiseAnd, BitwiseXor,
        Assign,
        Equals,  NotEquals,
        LessThan, GreaterThan, 
        LessEquals, GreaterEquals,
        In, NotIn, Is, IsNot,
        DictLookup,
        TmpAssignArg
    };
    
    enum class UnOp
    {
        NotTest,
        Star,
        StarStar,
        Positive,
        Negative,
        BitwiseNot,
        Await
    };
        
}

#endif // OP_H
