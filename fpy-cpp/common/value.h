//
// The Value class represents a single Python value. This can take any
// type a Python value can.
//

#ifndef VALUE_H
#define VALUE_H

#include <memory>
#include <vector>
#include <map>
#include <set>
#include <complex>


namespace fastpython {

    //
    // Value class.
    //
    
    class Value
    {
    public:
        //Value();
        virtual ~Value() = 0;
        
    };
    
    
    //
    // ValueNumber represents any kind of number: int, float or complex.
    //
    
    class ValueNumber : public Value
    {
    public:
        virtual ~ValueNumber() = 0;
    };
    
    
    //
    // ValueNumber represents 64-bit integer numbers.
    //
    
    class ValueInt : public ValueNumber
    {
    private:
        int64_t v_;
        
    public:
        explicit ValueInt(int64_t v) { v_ = v; }
    };
    
    
    //
    // ValueNumber represents floating point numbers.
    //
    
    class ValueFloat : public ValueNumber
    {
    private:
        double v_;
        
    public:
        explicit ValueFloat(double v) { v_ = v; }
    };
    
    
    //
    // ValueNumber represents complex numbers.
    //
    
    class ValueComplex : public ValueNumber
    {
    private:
        std::complex<double> v_;
        
    public:
        explicit ValueComplex(const std::complex<double> &v) { v_ = v; }
    };
    
    
    //
    // ValueStr represents strings.
    //
    
    class ValueStr : public Value
    {
    private:
        std::string v_;
        
    public:
        explicit ValueStr(const std::string &v) { v_ = v; }
    };
    
    
    //
    // ValueBoolean represents truth values.
    //
    
    class ValueBoolean : public Value
    {
    private:
        bool v_;
        
    public:
        explicit ValueBoolean(bool v) { v_ = v; }
    };
    
    
    //
    // ValueNone represents cases where a value isn't available.
    //
    
    class ValueNone : public Value
    {
    public:
        explicit ValueNone() {}
    };
    
    
    //
    // ValueTuple represents a fixed groups of values in a result.
    //
    
    class ValueTuple : public Value
    {
    private:
        std::vector<std::shared_ptr<Value>> v_;
        
    public:
        explicit ValueTuple(const std::vector< std::shared_ptr<Value> > &v) { v_ = v; }
    };
    
    
    //
    // ValueList represents groups of values which can be added to removed from.
    //
    
    class ValueList : public Value
    {
    private:
        std::vector<std::shared_ptr<Value>> v_;
        
    public:
        explicit ValueList(const std::vector< std::shared_ptr<Value> > &v) { v_ = v; }
    };
    
    
    //
    // ValueDict represents a mapping of names to values.
    //
    
    class ValueDict : public Value
    {
    private:
        std::map<std::string, std::shared_ptr<Value> > v_;
        
    private:
        explicit ValueDict(const std::map<std::string, std::shared_ptr<Value> > &v) { v_ = v; }
    };
    
    
    //
    // ValueSet represents a group of values with no duplicates allowed.
    //
    
    class ValueSet : public Value
    {
    private:
        std::set< std::shared_ptr<Value> > v_;
        
    private:
        explicit ValueSet(const std::set< std::shared_ptr<Value> > &v) { v_ = v; }
    };
    
    
    //
    // ValueBytes represents binary data.
    //
    
    class ValueBytes : public Value
    {
    private:
        std::vector<uint8_t> v_;
        
    public:
        explicit ValueBytes(const std::vector<uint8_t> &v) { v_ = v; }
    };
    
    
} // namespace fastpython

#endif // VALUE_H
