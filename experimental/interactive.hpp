#ifndef FPY_INTERACTIVE_HPP
#define FPY_INTERACTIVE_HPP

namespace fastpython {

class Interactive
{
public:
    Interactive();

    void start();
};

} // namespace fastpython

#endif // FPY_INTERACTIVE_HPP
