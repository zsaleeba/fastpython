#ifndef FPY_FILE_READER_HPP
#define FPY_FILE_READER_HPP

#include <string>


namespace fastpython {

class FileReader
{
public:
    FileReader(const std::string &fileName);

    void start();

private:
    std::string fileName_;
};

} // namespace fastpython

#endif // FPY_FILE_READER_HPP
