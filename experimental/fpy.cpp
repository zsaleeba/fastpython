#include <iostream>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <optional>
#include <vector>

#include <cppcoro/sync_wait.hpp>
#include <cppcoro/task.hpp>
#include <cppcoro/static_thread_pool.hpp>
#include <cppcoro/when_all.hpp>

#include "structopt.hpp"
#include "interactive.hpp"
#include "filereader.hpp"


// structopt-style arguments.
struct Options
{
    std::vector<std::string> files;
};
STRUCTOPT(Options, files);


#if 0
using namespace std::chrono_literals;

cppcoro::task<std::string> getFirst() {
    std::this_thread::sleep_for(1s);
    co_return "First";
}

cppcoro::task<std::string> getSecond() {
    std::this_thread::sleep_for(1s);
    co_return "Second";
}

cppcoro::task<std::string> getThird() {
    std::this_thread::sleep_for(1s);
    co_return "Third";
}

template <typename Func>
cppcoro::task<std::string> runOnThreadPool(cppcoro::static_thread_pool& tp, Func func) {
    co_await tp.schedule();
    auto res = co_await func();
    co_return res;
}

cppcoro::task<> runAll(cppcoro::static_thread_pool& tp) {
    
    auto [fir, sec, thi] = co_await cppcoro::when_all(    // (3)
        runOnThreadPool(tp, getFirst),
        runOnThreadPool(tp, getSecond), 
        runOnThreadPool(tp, getThird)
    );
    
    std::cout << fir << " " << sec << " " << thi << std::endl;
    
}

void demo()
{
    std::cout << std::endl;
    
    auto start = std::chrono::high_resolution_clock::now();

    cppcoro::static_thread_pool tp;                         // (1)
    cppcoro::sync_wait(runAll(tp));                         // (2)
    
    std::cout << std::endl;
    
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;    // (4)
    std::cout << "Execution time " << elapsed.count() << " seconds." << std::endl;
    
    std::cout << std::endl;
}
#endif

int main(int argc, char *argv[])
{
    //demo();

    // Parse the command line args.
    try {  
        auto options = structopt::app("fpy").parse<Options>(argc, argv);

        if (options.files.empty())
        {
            // If no files are specified, use the interactive mode.
            auto reader = fastpython::Interactive();
            reader.start();
        }
        else
        {
            // If files are specified, read them all simultaneously.
            std::vector<fastpython::FileReader> readers;
            for (auto f: options.files)
            {
                readers.emplace_back(f);
            }

            for (auto r: readers)
            {
                r.start();
            }
        }

    } catch (structopt::exception& e) {
        std::cout << e.what() << "\n";
        std::cout << e.help();
    }

    return EXIT_SUCCESS;
}
