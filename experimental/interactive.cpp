#include <string>
#include <iostream>

#include "interactive.hpp"


namespace fastpython {


Interactive::Interactive()
{
}

void Interactive::start()
{
    std::cout << "fastpython 0.0.1\n";
    std::cout << ">>> " << std::flush;

    std::string line;
    while (std::getline(std::cin, line))
    {
        std::cout << ">>> " << std::flush;
    }

    std::cout << std::endl;
}


} // namespace fastpython
