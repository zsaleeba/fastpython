#include <string>
#include <iostream>
#include <fstream>

#include "filereader.hpp"


namespace fastpython {


FileReader::FileReader(const std::string &fileName)
    : fileName_(fileName)
{
}


void FileReader::start()
{
    // Open the source file.
    std::fstream inFile;
    inFile.open(fileName_, std::ios::in);
    
    if (!inFile.is_open())
    {
        std::cerr << "can't open " << fileName_ << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // Read it line by line.
    std::string tp;
    while (getline(inFile, tp))
    { 
        std::cout << tp << "\n"; 
    }

    // Close.
    inFile.close();
}


} // namespace fastpython
