#!/usr/bin/python3
import antlr4
from antlr4.InputStream import InputStream
from antlr4.FileStream import FileStream
import parser
import sys


def main():
    input_stream = None
    if len(sys.argv) > 1:
        input_stream = FileStream(sys.argv[1])
    else:
        input_stream = InputStream(sys.stdin.readline())
        
    lexer = parser.Python3Lexer(input_stream)
    stream = antlr4.CommonTokenStream(lexer)
    pyparser = parser.Python3Parser(stream)
    parseTree = pyparser.file_input()
    ast = parser.Python3ParserVisitor().visitFile_input(parseTree)
    print(ast)

if __name__ == '__main__':
    main()
