from enum import Enum, auto


#
# Abstract Syntax Tree.
#

class AST(object):
    def __init__(self, lineno, col_offset, end_lineno, end_col_offset):
        self.lineno = lineno
        self.col_offset = col_offset
        self.end_lineno = end_lineno
        self.end_col_offset = end_col_offset

    def toString(self, ind):
        return type(self).__name__ + "\n"


#
# Modules.
#

class Mod(AST):
    pass

class Module(Mod):
    def __init__(self, body, type_ignores):
        self.body = body
        self.type_ignores = self.type_ignores

    def toString(self, ind):
        return "module\n" + dump(self.body, ind, name="body") + dump(self.type_ignores, ind, name="type_ignores")


class Interactive(Mod):
    def __init__(self, body):
        self.body = body

    def toString(self, ind):
        return "interactive\n" + self.body.dump(ind)

class Expression(Mod):
    def __init__(self, expr):
        self.expr = expr

    def toString(self, ind):
        return "expression\n" + self.expr.dump(ind)

class FunctionType(Mod):
    def __init__(self, expr, returns):
        self.expr = expr
        self.returns = returns

    def toString(self, ind):
        return "functionType\n" + self.expr.dump(ind, name="expr") + self.returns.dump(ind, name="returns")


#
# Statements.
#

class Stmt(AST):
    pass

class FunctionDef(Stmt):
    def __init__(self, name, args, body, decorator_list, returns, type_comment):
        self.name = name
        self.args = args
        self.body = body
        self.decorator_list = decorator_list
        self.returns = returns
        self.type_comment = type_comment

    def toString(self, ind):
        return "functionDef" + dump(self.name, ind, name="name") + dump(self.args, ind, name="args") + dump(self.body, ind, name="body") + dump(self.decorator_list, ind, name="decorator_list") + dump(self.returns, ind, name="returns") + dump(self.type_comment, ind, name="type_comment")

class ClassDef(Stmt):
    def __init__(self, name, bases, keywords, body, decorator_list):
        self.name = name
        self.bases = bases
        self.keywords = keywords
        self.body = body
        self.decorator_list = decorator_list

    def toString(self, ind):
        return "classDef" + dump(self.name, ind, name="name") + dump(self.bases, ind, name="bases") + dump(self.keywords, ind, name="keywords") + dump(self.body, ind, name="body") + dump(self.decorator_list, ind, name="decorator_list")

class Return(Stmt):
    def __init__(self, value):
        self.value = value

class Delete(Stmt):
    def __init__(self, targets):
        self.targets = targets

class Assign(Stmt):
    def __init__(self, targets, value, type_comment):
        self.targets = targets
        self.value = value
        self.type_comment = type_comment

class For(Stmt):
    def __init__(self, target, iter, body, orelse, type_comment):
        self.target = target
        self.iter = iter
        self.body = body
        self.orelse = orelse
        self.type_comment = type_comment

class While(Stmt):
    def __init__(self, test, body, orelse):
        self.test = test
        self.body = body
        self.orelse = orelse

class If(Stmt):
    def __init__(self, test, body, orelse):
        self.test = test
        self.body = body
        self.orelse = orelse

class With(Stmt):
    def __init__(self, items, body, type_comment):
        self.items = items
        self.body = body
        self.type_comment = type_comment

class Import(Stmt):
    def __init__(self, names):
        self.names = names

class ImportFrom(Stmt):
    def __init__(self, module, names, level):
        self.module = module
        self.names = names
        self.level = level

class ExprStmt(Stmt):
    def __init__(self, value):
        self.value = value

class Pass(Stmt):
    pass

class Break(Stmt):
    pass

class Continue(Stmt):
    pass


#
# Expressions.
#

class Expr(AST):
    pass

class BoolOpType(Enum):
    AND = auto()
    OR = auto()

class BoolOp(Expr):
    def __init__(self, op, values):
        self.op = op
        self.values = values

class BinOpType(Enum):
    ADD = auto()
    SUB = auto()
    MULT = auto()
    DIV = auto()
    MOD = auto()
    POW = auto()
    LSHIFT = auto()
    RSHIFT = auto()
    BIT_OR = auto()
    BIT_XOR = auto()
    BIT_AND = auto()

class BinOp(Expr):
    def __init__(self, left, op, right):
        self.left = left
        self.op = op
        self.right = right

class UnaryOpType(Enum):
    INVERT = auto()
    NOT = auto()
    UADD = auto()
    USUB = auto()

class UnaryOp(Expr):
    def __init__(self, op, operand):
        self.op = op
        self.operand = operand

class CompareOpType(Enum):
    EQ = auto()
    NOT_EQ = auto()
    LT = auto()
    LT_E = auto()
    GT = auto()
    GT_E = auto()
    IS = auto()
    IS_NOT = auto()
    IN = auto()
    NOT_IN = auto()

class Compare(Expr):
    def __init__(self, left, ops, comparators):
        self.left = left
        self.ops = ops
        self.comparators = comparators

class Call(Expr):
    def __init__(self, func, args, keywords):
        self.func = func
        self.args = args
        self.keywords = keywords

    def toString(self, ind):
        return "call" + self.func + "\n" + dump(self.args, ind, name="args") + dump(self.keywords, ind, name="keywords")

class Constant(Expr):
    def __init__(self, value, kind):
        self.value = value
        self.kind = kind

    def toString(self, ind):
        return "constant=" + self.value + "\n"

#
# Temporary nodes.
#

class TempAtomName(Expr):
    def __init__(self, name):
        self.name = name


#
# Visitor pattern for ASTs.
#

class Visitor:
    pass

#
# Dump an AST for debug purposes.
#

def dump(node, ind=0, name=None):
    if node and isinstance(node, AST):
        return ("  " * ind) + ((name + "=") if name else "") + node.toString(ind+1)
    else:
        return ""

